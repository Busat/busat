from django.db import models
from caja.models import Caja
from django_extensions.db.models import TimeStampedModel
from django.urls import reverse

#Modelo de tipo de movimiento
class TipoMovimiento(models.Model):
	descripcion = models.CharField(max_length = 20)

	def __str__(self):
		return self.descripcion
#Modelo Opcion
class Opcion(models.Model):
	descripcion = models.CharField(max_length = 50)
	tipo_movimiento = models.ForeignKey(TipoMovimiento, on_delete = models.CASCADE)

	def __str__(self):
		return self.descripcion
#Modelo Moviemiento
class Movimiento(TimeStampedModel):
	valor = models.IntegerField(default=0)
	tipo_movimiento = models.ForeignKey(TipoMovimiento, on_delete = models.CASCADE)
	opcion = models.ForeignKey(Opcion, on_delete = models.CASCADE)
	descripcion = models.TextField(max_length=150, blank=True, null=True)
	caja = models.ForeignKey(Caja, on_delete = models.CASCADE)

	def url_absoluta(self):
		return reverse("caja:resumen_caja", kwargs={'caja':self.caja.caja_actual().pk})
# Create your models here.


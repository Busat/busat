from django.core.management.base import BaseCommand, CommandError
from movimientos.models import TipoMovimiento, Opcion

class Command(BaseCommand):
	def handle(self, *args, **options):
		li_tipo = ['GASTO','RETIRO', 'INGRESO']
		li_opcion  = ['INGRESO DE CAJA', 'RETIRO DE CAJA', 'VARIOS', 'VALES','TRANSPORTE', 
		'TRAMITES','SUELDO REVISADOR', 'SUELDO AYUDANTE', 'SEGURO', 'REP TERJETAS', 'REC/CEL/COB',
		'PAPELERÍA', 'OFICINA', 'MÉDICO','MANTENIMIENTO MOTO', 'GASOLINA', 'COMISIÓN', 
		'AZULEJOS', 'AYUDANTE', 'ARRIENDO', 'ALIMENTACIÓN']
		if TipoMovimiento.objects.all().count() == 0:
			for data in li_tipo:
				objeto = TipoMovimiento(descripcion=data)
				objeto.save()
			for data in li_opcion:
				if data == "INGRESO DE CAJA":
					objeto = Opcion(descripcion=data, tipo_movimiento = TipoMovimiento.objects.get(descripcion="INGRESO"))
					objeto.save()
				if data == "RETIRO DE CAJA":
					objeto = Opcion(descripcion=data, tipo_movimiento = TipoMovimiento.objects.get(descripcion="RETIRO"))
					objeto.save()
				if data != "RETIRO DE CAJA" and data != "INGRESO DE CAJA":
					objeto = Opcion(descripcion=data, tipo_movimiento = TipoMovimiento.objects.get(descripcion="GASTO"))
					objeto.save()




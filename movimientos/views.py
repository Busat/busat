from django.shortcuts import render, redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic.edit import FormView, UpdateView, DeleteView, CreateView
from django.views.generic import ListView

from movimientos.models import Movimiento
from movimientos.forms import MovimientoCrearForm, MovimientoActualizarForm

class MovimientoCrear(LoginRequiredMixin, CreateView):
	model = Movimiento
	form_class = MovimientoCrearForm
	template_name = "bt_caja/caja_resumen.html"

	def get_success_url(self):
		return self.object.url_absoluta()

class MovimientoEliminar(LoginRequiredMixin, DeleteView):
	model = Movimiento
	success_url = reverse_lazy("clientes:listar_clientes")

	def get(self, request, *args, **kargs):
		objeto = Movimiento.objects.get(pk=self.kwargs['pk'])
		caja = objeto.caja.pk
		objeto.delete()
		return redirect("caja:resumen_caja", caja=caja)

class MovimientoActualizar(LoginRequiredMixin, UpdateView):
	model = Movimiento
	form_class = MovimientoActualizarForm
	template_name = "bt_movimientos/editar_movimiento.html"

	def get_success_url(self):
		return reverse_lazy("caja:resumen_caja", kwargs={'caja':self.object.caja.pk})
# Create your views here.

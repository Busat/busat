from django.contrib import admin
from movimientos.models import Movimiento, TipoMovimiento, Opcion

class MovimientoAdmin(admin.ModelAdmin):
	list_display = ['pk', 'valor','tipo_movimiento','opcion','descripcion','caja',]

class TipoMovimientoAdmin(admin.ModelAdmin):
	list_display = ['pk', 'descripcion' ]

class OpcionAdmin(admin.ModelAdmin):
	list_display = ['pk','descripcion', 'tipo_movimiento']

admin.site.register(Movimiento, MovimientoAdmin)
admin.site.register(TipoMovimiento, TipoMovimientoAdmin)
admin.site.register(Opcion, OpcionAdmin)
# Register your models here.

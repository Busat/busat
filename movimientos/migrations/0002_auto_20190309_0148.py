# Generated by Django 2.1 on 2019-03-09 01:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('movimientos', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='movimiento',
            old_name='Opcion',
            new_name='opcion',
        ),
        migrations.AlterField(
            model_name='movimiento',
            name='descripcion',
            field=models.TextField(blank=True, max_length=150, null=True),
        ),
    ]

from rest_framework.serializers import ModelSerializer

from movimientos.models import Movimiento, TipoMovimiento, Opcion

class MovimientoSerializer(ModelSerializer):
    class Meta:
        model = Movimiento
        fields = '__all__'

class TipoMovimientoSerializer(ModelSerializer):
    class Meta:
        model = TipoMovimiento
        fields = '__all__'

class OpcionSerializer(ModelSerializer):
    class Meta:
        model = Opcion
        fields = '__all__'

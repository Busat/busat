from django.urls import path

from movimientos.views import MovimientoCrear, MovimientoEliminar, MovimientoActualizar
from movimientos.ajax import get_opcion

app_name = "movimientos"

urlpatterns = [
	path('crear/', MovimientoCrear.as_view(), name = "crear_movimiento"),
	path('eliminar/<int:pk>',MovimientoEliminar.as_view(), name="eliminar_movimiento" ),
	path('editar/<int:pk>', MovimientoActualizar.as_view(), name="editar_movimiento"), 
	#Ajax
	path('opcion/', get_opcion, name = "get_opcion"),

]
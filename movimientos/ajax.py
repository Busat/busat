from django.http import JsonResponse
from movimientos.models import TipoMovimiento, Opcion
from django.views import View

def get_opcion(request):
	con = 0
	options = '<option value="" selected="selected" >--------------</option>'
	idTipoMov = request.GET.get('id_tipo_movi')
	opc = Opcion.objects.none()
	if idTipoMov:
		opc = Opcion.objects.filter(descripcion__contains=idTipoMov)
		if not opc:
			opc = Opcion.objects.all().exclude(descripcion__in = ['RETIRO DE CAJA', 'INGRESO DE CAJA'])
	con = 0
	for opcion in opc:
		con+=1
		if con==1:
			options = '<option value="%d" selected="selected" >%s</option>'%(opcion.pk, opcion.descripcion)
		else:
			options += '<option value="%d">%s</option>'%(opcion.pk, opcion.descripcion)
	reponse = {}
	reponse['opc'] = options
	return JsonResponse(reponse)
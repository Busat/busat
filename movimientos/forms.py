from django import forms

from movimientos.models import Movimiento, Opcion

class MovimientoCrearForm(forms.ModelForm):
	class Meta:
		model = Movimiento
		fields = ['valor','tipo_movimiento','opcion','descripcion','caja',]
		widgets = {
			'valor': forms.TextInput(attrs = {'class':'form-control', 'type':'number'}),
			'tipo_movimiento': forms.Select(attrs={'class':'form-control', 'onchange':'get_opciones()'}),
			'opcion': forms.Select(attrs={'class':'form-control'}),
			'descripcion': forms.Textarea(attrs= {'class':'form-control'}),
			'caja': forms.Select(attrs={'class':'form-control'}),
		}

class MovimientoActualizarForm(forms.ModelForm):
	class Meta:
		model = Movimiento
		fields = ['valor','tipo_movimiento','opcion','descripcion',]
		widgets = {
			'valor': forms.TextInput(attrs = {'class':'form-control', 'type':'number'}),
			'tipo_movimiento': forms.Select(attrs={'class':'form-control', 'onchange':'get_opciones()'}),
			'opcion': forms.Select(attrs={'class':'form-control'}),
			'descripcion': forms.Textarea(attrs= {'class':'form-control'}),	
		}
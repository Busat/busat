from django.contrib import admin
from usuarios.models import User
from rutas.models import Comunidad

#Muestra usuario en el administrador
class UsuarioAdmin(admin.ModelAdmin):
	list_display= ['pk','username','first_name','last_name', 'email', 'comunidad',]

#Muestra comunidad en el administrador
class ComunidadAdmin(admin.ModelAdmin):
	list_display = ['pk', 'nombre','estado']

admin.site.register(User, UsuarioAdmin)
admin.site.register(Comunidad, ComunidadAdmin)
# Register your models here.

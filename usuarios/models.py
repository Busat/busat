from django.db import models
from django.utils.translation import ugettext as _
from django.contrib.auth.models import AbstractUser, Permission, Group
from rutas.models import Ruta, Comunidad
from django_extensions.db.models import TimeStampedModel

#Modelo User
class User(AbstractUser):
	comunidad = models.ForeignKey(Comunidad, on_delete=models.CASCADE, null=True, blank=True)
	rutas = models.ManyToManyField(Ruta, blank=True)
	class Meta:
		permissions = (
			('is_superadmin', _('Super Usuario')),
			('is_admincomunidad', _('Administrador Comunidad')),
			('is_administradorsecre', _('Administrador de Secretarias')),
			('is_secretarias', _('Secretarias')),
			('crear_clientes', _('Crear Clientes')),
			('eliminar_clientes', _('Eliminar Clientes')),
			('inactivar_clientes', _('Inactivar Clientes')),
			('agregar_movimientos', _('Agregar Movimientos')),
			('eliminar_movimientos', _('Eliminar Movimientos')),
			('gestionar_pagos', _('Gestionar Pagos')),
			('elminar_pagos', _('Eliminar Pagos')),
			('cerrar_rutas', _('Cerrar Rutas')),
			('abrir_rutas', _('Abrir Rutas')),
			('eliminar_rutas', _('Eliminar Rutas')),
			('crear_ventas', _('Crear Ventas')),
			('editar_ventas', _('Editar Ventas')),
			('crear_rutas', _('Crear Rutas')),
			#('cambiar_cliente_ventas', _('Cambiar clientes de ventas')),
			('asignar_permiso_apertura_multiple', _('Asignar permiso de apertura multiple')),
			#('asignar_permiso_creacion_clientes', _('Asignar permiso de creacion de clientes')),
			#('asignar_permiso_visita_obligatoria', _('Asignar permiso de visita obligatoria')),
			)
		
	#Esta funcion nos devuelve un True o un False si el usuario del que se llama la funcion Tiene este permiso
	def admin_comunidad(self):
		return self.has_perm('usuarios.is_admincomunidad')

	#Esta funcion nos devuelve un True o un False si el usuario del que se llama la funcion Tiene este permiso
	def admin_secretaria(self):
		return self.has_perm('usuarios.is_administradorsecre')

	#Esta funcion nos devuelve un True o un False si el usuario del que se llama la funcion Tiene este permiso
	def secretaria(self):
		return self.has_perm('usuarios.is_secretarias')

	def permisos(self):
		li = []
		for contenido in self.get_all_permissions():
			model, permiso = contenido.split(".")
			li.append(permiso)
		return li
#Modelo que se genera de la relacion ManyToManyField de django, la cual esta con Ruta a Usuario
class UserRutas(models.Model):
    user = models.ForeignKey(User, models.DO_NOTHING)
    ruta = models.ForeignKey(Ruta, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'usuarios_user_rutas'
        unique_together = (('user', 'ruta'),)


"""class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)"""
# Create your models here.

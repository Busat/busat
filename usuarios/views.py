from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect, HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic.edit import FormView, UpdateView
from usuarios.forms import AuthenticationForm, UserCreationForm2, ComunidadForm, UserCreationForm, UserUpdateForm, UsuarioRutaForm
from django.contrib import auth
from django.contrib.auth.models import Permission, Group
from django.contrib.auth import login, update_session_auth_hash
from django.views.generic.edit import CreateView
from usuarios.models import  User
from rutas.models import Comunidad
from django.views.generic import ListView, TemplateView
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib import messages


class ComuinidadCrear(LoginRequiredMixin, CreateView, PermissionRequiredMixin):
	permission_required = 'usuarios.is_superadmin'
	model = Comunidad
	form_class = ComunidadForm
	template_name = "bt_usuarios/crear_comunidad.html"
	success_url = reverse_lazy("usuarios:listar_comunidad")

class UsuarioEditar(LoginRequiredMixin, UpdateView):
	model = User
	form_class = UserUpdateForm
	template_name = "bt_usuarios/editar_usuario.html"
	success_url = reverse_lazy("usuarios:listar_usuarios")

class ComuinidadListar(LoginRequiredMixin, ListView):
	model = Comunidad
	template_name = "bt_usuarios/listar_comunidad.html"

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		form = ComunidadForm
		context['form'] = form
		return context

class PermisosUsuario(LoginRequiredMixin, TemplateView):
	template_name = "bt_usuarios/gestionar_permisios_rutas.html"

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['form'] = UsuarioRutaForm(initial={'user':self.request.user.comunidad})
		try:
			context['perfil'] = User.objects.get(pk=self.kwargs['user'])
			li = ['is_superadmin',
					'is_admincomunidad',
					'is_administradorsecre',
					'is_secretarias',
					'crear_clientes',
					'eliminar_clientes',
					'inactivar_clientes',
					'agregar_movimientos',
					'eliminar_movimientos',
					'gestionar_pagos',
					'elminar_pagos',
					'cerrar_rutas',
					'eliminar_rutas',
					'crear_ventas',
					'editar_ventas',
					'asignar_permiso_apertura_multiple', 
					'crear_rutas',
					'abrir_rutas']
			context['permisos_secre'] = Permission.objects.filter(codename__in=[contenido for contenido in li])
		except User.DoesNotExist:
			messages.success(self.request, 'Este usuario no tiene permisos de secretaria, contactese con un un administrador')
		except Group.DoesNotExist:
			messages.success(self.request, 'Contactese con un administrador por que el grupo de secretarias no esta creado.')
		return context

class UsuarioRutaActualizar(LoginRequiredMixin, UpdateView):
	model = User
	template_name = "bt_usuarios/gestionar_permisios_rutas.html"
	form_class = UsuarioRutaForm

	def get_success_url(self):
		usuario = User.objects.get(pk=self.kwargs['pk'])
		data = usuario.pk
		return reverse_lazy("usuarios:gestionar_usuario", kwargs={'user':data})

class UsuarioCrear(LoginRequiredMixin, CreateView):
	model = User
	form_class = UserCreationForm
	template_name = "bt_usuarios/crear_usuarios.html"
	success_url = reverse_lazy("usuarios:listar_usuarios")
	def post(self, request, *args, **kwargs):
		form = self.form_class(request.POST)
		if form.is_valid():
			obj = Permission.objects.get(name__contains="Administrador Comunidad")
			first_name = form.cleaned_data.get("first_name")
			username = form.cleaned_data.get("username")
			comunidad = form.cleaned_data.get("comunidad")
			print(comunidad)
			comunidad = Comunidad.objects.get(nombre=comunidad)
			password = form.cleaned_data.get("password1")
			user = User(first_name=first_name, username=username, comunidad=comunidad)
			user.set_password(password)
			user.save()
			user.user_permissions.add(obj.pk)
		return redirect("usuarios:listar_usuarios")

class UsuarioCrearAdmin(LoginRequiredMixin, CreateView):
	model = User
	form_class = UserCreationForm2
	template_name = "bt_usuarios/crear_usuarios.html"
	success_url = reverse_lazy("usuarios:listar_usuarios")
	def post(self, request, *args, **kwargs):
		form = self.form_class(request.POST)
		if form.is_valid():
			permiso = int(form.cleaned_data.get("status"))
			if permiso==1:
				obj = Permission.objects.get(name__contains="Administrador de Secretarias")
				first_name = form.cleaned_data.get("first_name")
				username = form.cleaned_data.get("username")
				comunidad = Comunidad.objects.get(pk=self.request.user.comunidad.pk)
				password = form.cleaned_data.get("password1")
				user = User(first_name=first_name, username=username, comunidad=comunidad)
				user.set_password(password)
				user.save()
				user.user_permissions.add(obj.pk)
			elif permiso==2:
				obj = Permission.objects.get(name="Secretarias")
				first_name = form.cleaned_data.get("first_name")
				username = form.cleaned_data.get("username")
				comunidad = Comunidad.objects.get(pk=self.request.user.comunidad.pk)
				password = form.cleaned_data.get("password1")
				user = User(first_name=first_name, username=username, comunidad=comunidad)
				user.set_password(password)
				user.save()
				user.user_permissions.add(obj.pk)
		return redirect("usuarios:listar_usuarios")

class UsuariosListar(LoginRequiredMixin, ListView):
	model = User
	template_name = "bt_usuarios/listar_usuarios.html"

	def get_queryset(self):
		if self.request.user.has_perm('usuarios.is_superadmin'):
			return User.objects.all()
		else:
			return User.objects.filter(comunidad=self.request.user.comunidad.pk)

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		if self.request.user.has_perm('usuarios.is_superadmin'):
			form = UserCreationForm({'user':self.request.user},{'nada':'nada'})
		else:
			form = UserCreationForm2({'user':self.request.user},{'nada':'nada'})
		context['form'] = form
		return context

class Login(FormView):
	#Establecemos la plantilla a utilizar
	template_name = 'base/login.html'
	#Le indicamos que el formulario a utilizar es el formulario de autenticación de Django
	form_class = AuthenticationForm
	#Le decimos que cuando se haya completado exitosamente la operación nos redireccione a la url bienvenida de la aplicación personas
	success_url =  reverse_lazy("rutas:listar_rutas")

	def dispatch(self, request, *args, **kwargs):
        #Si el usuario está autenticado entonces nos direcciona a la url establecida en success_url
		if request.user.is_authenticated:
			if not request.user.is_superuser:
				return HttpResponseRedirect(self.get_success_url())
			else:
				return redirect('usuarios:listar_usuarios')
		#Sino lo está entonces nos muestra la plantilla del login simplemente
		else:
			return super(Login, self).dispatch(request, *args, **kwargs)

	def form_valid(self, form):
		login(self.request, form.get_user())
		return super(Login, self).form_valid(form)

def logout(request):
    auth.logout(request)
    # Redirect to a success page.
    return redirect("login")

class PolitivasPrivacidad(TemplateView):
	template_name = "base/politicas_privacidad.html"
# Create your views here.

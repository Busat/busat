from usuarios.models import User
from django.core.management.base import BaseCommand

class Command(BaseCommand):

    def handle(self, *args, **options):
    	try:
	    	dato = int(input('Ingrese el pk del usuario \n'))
	    	password = input('Ingrese la contraseña \n')
	    	user = User.objects.get(pk=dato)
	    	user.set_password(dato)
	    	user.save()
	    	print("La contraseña fue guardada")
    	except User.DoesNotExist:
    		print("No encuntramos el usuario")
    	except User.MultipleObjectsReturned:
    		print("No encuentramos el usuario")


from django.contrib.auth.models import Permission, Group
from usuarios.models import User
from django.core.management.base import BaseCommand


class Command(BaseCommand):
	"""
	Programa que resetea los permisos de un usuario segun su roll
	"""

	def handle(self, *args, **options):
		print("Initial...")
		# Opteniendo todos lo usuarios
		users = User.objects.all()
		print(users)
		perms = Permission.objects.filter(codename__in=['crear_clientes',
														'eliminar_clientes',
														'inactivar_clientes',
														'agregar_movimientos',
														'eliminar_movimientos',
														'gestionar_pagos',
														'elminar_pagos',
														'cerrar_rutas',
														'eliminar_rutas',
														'crear_ventas',
														'editar_ventas',
														'crear_rutas',
														'abrir_rutas'])
		for data in users:
			if data.has_perm('usuarios.is_admincomunidad') or data.has_perm('usuarios.is_administradorsecre'):
				for perm in perms:
					data.user_permissions.add(perm.pk)


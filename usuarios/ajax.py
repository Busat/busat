from usuarios.models import User
from rutas.models import Comunidad
from django.contrib.auth.models import Permission
from django.http import JsonResponse

#Funcion AJAX que nos permite actualizar el estado de una comunidad
def cambiar_estado_comunidad(request):
	pk = request.GET.get("id_comunidad")
	objecto = Comunidad.objects.get(pk=pk)
	if objecto.estado == True:
		objecto.estado=False
	else:
		objecto.estado=True
	objecto.save()
	return JsonResponse({'data':'success'})

#Funcion AJAX que nos permite actualizar el estado de un usuario
def cambiar_estado_usuario(request):
	pk = request.GET.get('id_usuario')
	objecto = User.objects.get(pk=pk)
	if objecto.is_active == True:
		objecto.is_active=False
	else:
		objecto.is_active=True
	objecto.save()
	return JsonResponse({'data':'success'})

def cambiar_permisos_secretaria(request):
	pk_permiso = request.GET.get("id_permiso")
	pk_usuario = request.GET.get("id_usuario")
	user = User.objects.get(pk=int(pk_usuario))
	permiso = Permission.objects.get(pk = int(pk_permiso))
	if user.has_perm(("usuarios."+permiso.codename)):
		user.user_permissions.remove(permiso)
	else: 
		user.user_permissions.add(permiso)
	return JsonResponse({'opc':True})
# Generated by Django 2.1 on 2019-03-02 23:54

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('usuarios', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='user',
            options={'permissions': (('is_superadmin', 'Super Usuario'), ('is_admincomunidad', 'Administrador Comunidad'), ('is_administradorsecre', 'Administrador de Secretarias'), ('is_secretarias', 'Secretarias'), ('crear_clientes', 'Crear Clientes'), ('eliminar_clientes', 'Eliminar Clientes'), ('inactivar_clientes', 'Inactivar Clientes'), ('agregar_movimientos', 'Agregar Movimientos'), ('eliminar_movimientos', 'Eliminar Movimientos'), ('gestionar_pagos', 'Gestionar Pagos'), ('elminar_pagos', 'Eliminar Pagos'), ('cerrar_rutas', 'Cerrar Rutas'), ('eliminar_rutas', 'Eliminar Rutas'), ('crear_ventas', 'Crear Ventas'), ('editar_ventas', 'Editar Ventas'), ('asignar_permiso_apertura_multiple', 'Asignar permiso de apertura multiple'))},
        ),
    ]

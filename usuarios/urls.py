from usuarios.views import *
from django.urls import path
from usuarios.views import ComuinidadCrear, ComuinidadListar, UsuariosListar, \
								UsuarioCrear, UsuarioCrearAdmin, PermisosUsuario, UsuarioRutaActualizar
from usuarios.ajax import cambiar_estado_comunidad, cambiar_estado_usuario, \
							cambiar_permisos_secretaria
app_name = "usuarios"

urlpatterns = [
	#URLS de app Usuarios
	path('crear-comunidad/',ComuinidadCrear.as_view(), name="crear_comunidad"),
	path('listar-comunidad/',ComuinidadListar.as_view(), name="listar_comunidad"),
	path('listar-usuarios/',UsuariosListar.as_view(), name = "listar_usuarios" ),
	path('crear-usuarios/', UsuarioCrear.as_view(), name = "crear_usuarios"),
	path('crear-usuarios/admin', UsuarioCrearAdmin.as_view(), name = "crear_usuarios_admin"),
	path('editar-usuarios/<int:pk>/', UsuarioEditar.as_view(), name = "editar_usuario"),
	path('gestionar/<int:user>/', PermisosUsuario.as_view(), name="gestionar_usuario" ),
	path('agregar/rutas/secretaria/<int:pk>', UsuarioRutaActualizar.as_view(), name="rutas_secretaria"),

	#URLS de ajax Usuarios cambiar_estado_usuario UsuarioEditar
	path('estado/comunidad/',cambiar_estado_comunidad, name="cambiar_estado_comunidad" ),
	path('estado/usuario/',cambiar_estado_usuario, name="cambiar_estado_usuario" ),
	path('permisos/usuario/', cambiar_permisos_secretaria, name="cambiar_permisos_secretaria")
]
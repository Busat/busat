from django.utils.translation import gettext, gettext_lazy as _
TIPO_USUARIO = (
	(1, ("Administrador Secretarias")),
	(2, ("Secretaria")),
)
TIPO_USUARIO_SUPER_ADMIN = (
	(1, ("Administrador de Comunidades")),
)
import unicodedata
from django import forms
from django.utils.text import capfirst
from django.utils.translation import gettext, gettext_lazy as _
from django.contrib.auth import (
    authenticate, get_user_model, password_validation,
)
UserModel = get_user_model()
from usuarios.choices import *
from usuarios.models import User
from rutas.models import Ruta
from rutas.models import Comunidad

class UsuarioRutaForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['rutas']

    def __init__(self, *args, **kwargs):
        super(UsuarioRutaForm, self).__init__(*args, **kwargs)
        if 'user' in kwargs['initial']:
            self.fields['rutas'].queryset = Ruta.objects.filter(imei__comunidad=(kwargs['initial']['user'].pk))

#Formulario de solo el campo de username (Crear usuario)
class UsernameField(forms.CharField):
    def to_python(self, value):
        return unicodedata.normalize('NFKC', super().to_python(value))


#Formulario de actualizar usuario
class UserUpdateForm(forms.ModelForm):
    """
    A form that creates a user, with no privileges, from the given username and
    password.
    """
    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
    }
    password1 = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.TextInput(attrs={'class':'form-control', 'type':'password', 'id':'field-4', 'placeholder':'Contraseña'}),
        help_text=password_validation.password_validators_help_text_html(),
    )
    password2 = forms.CharField(
        label=_("Password confirmation"),
        widget=forms.TextInput(attrs={'class':'form-control', 'type':'password', 'id':'field-5', 'placeholder':'Confirmar Contraseña'}),
        strip=False,
        help_text=_("Enter the same password as before, for verification."),
    )

    class Meta:
        model = User
        field = ("first_name")
        exclude = ('last_name','email','comunidad','username','is_superuser','is_staff','is_active','last_login','date_joined','password','groups','user_permissions', 'rutas')
        labels = {
            'first_name':'Nombre',

        }
        widgets ={
            'first_name':forms.TextInput(attrs={'class':'form-control', 'id':'field-2', 'placeholder':'Nombre', 'value':""}),
        }

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2

    def _post_clean(self):
        super()._post_clean()
        # Validate the password after self.instance is updated with form data
        # by super().
        password = self.cleaned_data.get('password2')
        if password:
            try:
                password_validation.validate_password(password, self.instance)
            except forms.ValidationError as error:
                self.add_error('password2', error)

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user

#Formulario de crear usuario con roll Super Administrador
class UserCreationForm(forms.ModelForm):
    """
    A form that creates a user, with no privileges, from the given username and
    password.
    """
    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
    }
    password1 = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.TextInput(attrs={'class':'form-control', 'type':'password', 'id':'field-4', 'placeholder':'Contraseña'}),
        help_text=password_validation.password_validators_help_text_html(),
    )
    password2 = forms.CharField(
        label=_("Password confirmation"),
        widget=forms.TextInput(attrs={'class':'form-control', 'type':'password', 'id':'field-5', 'placeholder':'Confirmar Contraseña'}),
        strip=False,
        help_text=_("Enter the same password as before, for verification."),
    )
    status = forms.ChoiceField(
        choices = TIPO_USUARIO_SUPER_ADMIN,
        label=_("Tipo Usuario"),
        widget=forms.Select(attrs={'class':'form-control','id':'field-5', 'requered':''}),
    )

    class Meta:
        model = User
        fields = ("comunidad","first_name","username")
        field_classes = {'username': UsernameField}
        labels = {
            'first_name':'Nombre',
            'username':'Login',

        }
        widgets ={
            'comunidad':forms.Select(attrs={'class':'form-control', 'id':'field-0',}),
            'first_name':forms.TextInput(attrs={'class':'form-control', 'id':'field-2', 'placeholder':'Nombre'}),
            'username':forms.TextInput(attrs={'class':'form-control','id':'field-3', 'placeholder':'Login'}),
        }
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if len(args)==2:
            print(len(args))
            user=args[0]['user']
        if self._meta.model.USERNAME_FIELD in self.fields:
            self.fields[self._meta.model.USERNAME_FIELD].widget.attrs.update({'autofocus': True})

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2

    def _post_clean(self):
        super()._post_clean()
        # Validate the password after self.instance is updated with form data
        # by super().
        password = self.cleaned_data.get('password2')
        if password:
            try:
                password_validation.validate_password(password, self.instance)
            except forms.ValidationError as error:
                self.add_error('password2', error)

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user

#Formulario de Crear usuario con roll Administrador Comunidad
class UserCreationForm2(forms.ModelForm):
    """
    A form that creates a user, with no privileges, from the given username and
    password.
    """
    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
    }
    password1 = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.TextInput(attrs={'class':'form-control', 'type':'password', 'id':'field-4', 'placeholder':'Contraseña'}),
        help_text=password_validation.password_validators_help_text_html(),
    )
    password2 = forms.CharField(
        label=_("Password confirmation"),
        widget=forms.TextInput(attrs={'class':'form-control', 'type':'password', 'id':'field-5', 'placeholder':'Confirmar Contraseña'}),
        strip=False,
        help_text=_("Enter the same password as before, for verification."),
    )
    status = forms.ChoiceField(
        choices = TIPO_USUARIO,
        label=_("Tipo Usuario"),
        widget=forms.Select(attrs={'class':'form-control','id':'field-5', 'requered':''}),
    )

    class Meta:
        model = User
        fields = ("comunidad","first_name","username")
        field_classes = {'username': UsernameField}
        labels = {
            'comunidad':'Comunidad',
            'first_name':'Nombre',
            'username':'Login',

        }
        widgets ={
            'comunidad':forms.Select(attrs={'class':'form-control', 'id':'field-1',}),
            'first_name':forms.TextInput(attrs={'class':'form-control', 'id':'field-2', 'placeholder':'Nombre'}),
            'username':forms.TextInput(attrs={'class':'form-control','id':'field-3', 'placeholder':'Login'}),
        }
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self._meta.model.USERNAME_FIELD in self.fields:
            self.fields[self._meta.model.USERNAME_FIELD].widget.attrs.update({'autofocus': True})

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2

    def _post_clean(self):
        super()._post_clean()
        # Validate the password after self.instance is updated with form data
        # by super().
        password = self.cleaned_data.get('password2')
        if password:
            try:
                password_validation.validate_password(password, self.instance)
            except forms.ValidationError as error:
                self.add_error('password2', error)

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user

#Formulario de autenticacion (Login)
class AuthenticationForm(forms.Form):
    """
    Base class for authenticating users. Extend this to get a form that accepts
    username/password logins.
    """
    username = UsernameField(
        max_length=254,
        widget=forms.TextInput(attrs={'autofocus': True}),
    )
    password = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.PasswordInput,
    )

    error_messages = {
        'invalid_login': _(
            "Por favor ingrese un nombre de usuario y una contraseña conrrecta. Recuerde que los campos son sensibles a las mayúsculas."
        ),
        'inactive': _("This account is inactive."),
    }
    error_comunidad={
        'comunidad': _(
            "Este usuario no tiene una comunidad"
            ),
        'inactiva': _(
            "La comuniadad de este usuario no esta activa, contactese con el administrador"
            ),
    }

    def __init__(self, request=None, *args, **kwargs):
        """
        The 'request' parameter is set for custom auth use by subclasses.
        The form data comes in via the standard 'data' kwarg.
        """
        self.request = request
        self.user_cache = None
        super().__init__(*args, **kwargs)

        # Set the label for the "username" field.
        self.username_field = UserModel._meta.get_field(UserModel.USERNAME_FIELD)
        if self.fields['username'].label is None:
            self.fields['username'].label = capfirst(self.username_field.verbose_name)

    def clean_username(self):
        username = self.cleaned_data.get('username')
        user = User.objects.filter(username=username).first()
        if user:
            if user.comunidad:
                if not user.comunidad.estado:
                    raise forms.ValidationError("La comuniadad de este usuario no esta activa, contactese con el administrador")
            else:
                if not user.is_superuser:
                    raise forms.ValidationError("Este usuario no tiene una comunidad")
        return username
    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username is not None and password:
            self.user_cache = authenticate(self.request, username=username, password=password)
            if self.user_cache is None:
                raise forms.ValidationError(
                    self.error_messages['invalid_login'],
                    code='invalid_login',
                    params={'username': self.username_field.verbose_name},
                )
            else:
                self.confirm_login_allowed(self.user_cache)

        return self.cleaned_data

    def confirm_login_allowed(self, user):
        """
        Controls whether the given User may log in. This is a policy setting,
        independent of end-user authentication. This default behavior is to
        allow login by active users, and reject login by inactive users.

        If the given user cannot log in, this method should raise a
        ``forms.ValidationError``.

        If the given user may log in, this method should return None.
        """
        if not user.is_active:
            raise forms.ValidationError(
                self.error_messages['inactive'],
                code='inactive',
            )

    def get_user_id(self):
        if self.user_cache:
            return self.user_cache.id
        return None

    def get_user(self):
        return self.user_cache


class SetPasswordForm(forms.Form):
    """
    A form that lets a user change set their password without entering the old
    password
    """
    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
    }
    new_password1 = forms.CharField(
        label=_("New password"),
        widget=forms.TextInput(attrs={'class':'form-control', 'type':'password'}),
        strip=False,
        help_text=password_validation.password_validators_help_text_html(),
    )
    new_password2 = forms.CharField(
        label=_("New password confirmation"),
        strip=False,
        widget=forms.TextInput(attrs={'class':'form-control', 'type':'password'}),
    )

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super().__init__(*args, **kwargs)

    def clean_new_password2(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                    self.error_messages['password_mismatch'],
                    code='password_mismatch',
                )
        password_validation.validate_password(password2, self.user)
        return password2

    def save(self, commit=True):
        password = self.cleaned_data["new_password1"]
        self.user.set_password(password)
        if commit:
            self.user.save()
        return self.user

#Formulario de crear Comunidad
class ComunidadForm(forms.ModelForm):
    class Meta:
        model = Comunidad
        fields=['nombre']
        label = {
            'nombre':'Nombre de la comunidad',
        }
        widgets = {
            'nombre': forms.TextInput(attrs={'class':'form-control','required':'true','id':'field-1', 'placeholder':'Comunidad'}),
        }

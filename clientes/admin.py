from django.contrib import admin
from clientes.models import Cliente, ClienteRuta

#Lista todos los Clientes en el admin de django
class ClienteAdmin(admin.ModelAdmin):
	list_display=['pk','nombre','apellido','alias','direccion','celular','cpf']


class ClienteRutaAdmin(admin.ModelAdmin):
	list_display = ['cliente', 'ruta',]

admin.site.register(Cliente, ClienteAdmin)
admin.site.register(ClienteRuta, ClienteRutaAdmin)

# Register your models here.

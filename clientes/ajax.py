from django.http import JsonResponse
from clientes.models import ClienteRuta

def editar_estado(request):
	cliente = ClienteRuta.objects.get(pk=request.GET.get("id_cliente"))
	if cliente.activo:
		cliente.activo = False
		cliente.save()
	else:
		cliente.activo = True
		cliente.save()
	return JsonResponse({'opc':True})
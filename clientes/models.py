from django.db import models
from rutas.models import Ruta
from django_extensions.db.models import TimeStampedModel

#Modelo Cliente
class Cliente(TimeStampedModel):
    nombre = models.CharField(max_length=70)
    apellido = models.CharField(max_length=50, blank=True, null=True)
    alias = models.CharField(max_length=50, blank=True, null=True)
    direccion = models.CharField(max_length=100, blank=True, null=True)
    celular = models.CharField(max_length=20, blank=True, null=True)
    cpf = models.CharField(max_length=20)
    ciudad = models.CharField(max_length=40,null=True, blank=True, default='')
    ruta = models.ManyToManyField(Ruta, through='ClienteRuta', blank=True)

    def __str__(self):
        return self.full_name()        

    def full_name(self):
        return "%s %s"%(self.nombre, self.apellido)

    def url_absoluta(self):
        return reverse("ventas:listar_todas_ventas", kwargs={'id_venta':self.venta.pk})
#Modelo que sale del ManyToManyField entre ruta y cliente

class ClienteRuta(TimeStampedModel):
    cliente = models.ForeignKey(Cliente, models.CASCADE)
    ruta = models.ForeignKey(Ruta, models.CASCADE)
    activo = models.BooleanField(default=True)

    def sin_venta_activa(self):
        data = self.venta_set.filter().order_by("-pk").first()
        if data:
            if not data.no_cobrado:
                return True
            else:
                return False

    def ultima_venta(self):
        data = self.venta_set.filter().order_by("-pk").first()
        return data

    def full_name(self):
        return "%s %s"%(self.cliente.nombre, self.cliente.apellido)

    def ventas_activas(self):
    	venta = self.ultima_venta()
    	if venta:
    		if venta.no_cobrado:
    			return True
    		else:
    			return False
    	else:
    		return False

    def se_renueva(self):
        from ventas.models import Venta
        venta = self.ultima_venta()
        if venta:
            ultimo_pago = venta.pago_set.all().order_by("-pk").first()
            if ultimo_pago.caja.pk == self.ruta.ultima_caja().pk:
                return False
            else:
                return True
        else:
            return True

# Create your models here.

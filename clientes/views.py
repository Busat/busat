from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from clientes.models import Cliente, ClienteRuta
from clientes.forms import ClienteForm, ClienteEditarForm
from ventas.forms import VentaForm
from rutas.models import Ruta


from django.urls import reverse_lazy
from django.views.generic.edit import FormView, UpdateView, DeleteView, CreateView
from django.views.generic import ListView

class ClienteCrear(LoginRequiredMixin, CreateView):
	model = Cliente
	form_class = ClienteForm
	template_name = "bt_clientes/editar_cliente.html"
	success_url = reverse_lazy("clientes:listar_clientes")

	def post(self, request, *args, **kwargs):
		context= {}
		form = self.form_class(request.POST)
		if form.is_valid():
			ruta = Ruta.objects.get(pk=int(form.cleaned_data.get("ruta")))
			cpf = form.cleaned_data.get("cpf")
			try:
				cliente = Cliente.objects.get(cpf=cpf)
				validator = ClienteRuta.objects.get(cliente=cliente, ruta=ruta.pk)
				messages.info(request, 'Ya existe este cliente en esta ruta!')
				context['form'] = form
				return redirect("clientes:listar_clientes", id_ruta=ruta.pk)
			except ClienteRuta.DoesNotExist:
				objeto = form.save()
				instance = ClienteRuta(cliente=cliente, ruta=ruta)
				instance.save()
				return redirect("clientes:listar_clientes", id_ruta=ruta.pk)
			except ClienteRuta.MultipleObjectsReturned:
				messages.info(request, 'Ya existe este cliente en esta ruta!')
				context['form'] = form
				return redirect("clientes:listar_clientes", id_ruta=ruta.pk)
			except Cliente.DoesNotExist:
				objeto = form.save()
				instance = ClienteRuta(cliente=objeto, ruta=ruta)
				instance.save()
				return redirect("clientes:listar_clientes", id_ruta=ruta.pk)
		else:
			context['form'] = form
			return render(request, "bt_clientes/listar_clientes.html", context)

class ClienteListar(LoginRequiredMixin, ListView):
	model = ClienteRuta
	template_name = "bt_clientes/listar_clientes.html"

	def get_queryset(self, **kwargs):
		queryset = super().get_queryset()
		return queryset.filter(ruta=self.kwargs['id_ruta'])

	def get_context_data(self, **kwargs):
		context = super().get_context_data()
		form = ClienteForm({'id_ruta':self.kwargs['id_ruta']})
		form2 = VentaForm
		context['id_ruta'] = self.kwargs['id_ruta']
		context['form']=form
		context['form2']=form2
		return context

class ClienteSinVentaActiva(LoginRequiredMixin, ListView):
	model = ClienteRuta
	template_name = "bt_clientes/listar_clientes_sin_venta_activa.html"

	def get_queryset(self, **kwargs):
		queryset = super().get_queryset()
		return queryset.filter(ruta=self.kwargs['id_ruta'])

	def get_context_data(self, **kwargs):
		context = super().get_context_data()
		form = ClienteForm({'id_ruta':self.kwargs['id_ruta']})
		form2 = VentaForm
		context['id_ruta'] = self.kwargs['id_ruta']
		return context

class ClienteActualizar(LoginRequiredMixin, UpdateView):
	model = Cliente
	form_class = ClienteEditarForm
	template_name = "bt_clientes/editar_cliente.html"

	def get_success_url(self):
		return reverse_lazy("clientes:listar_clientes", kwargs={'id_ruta':self.kwargs['ruta']})

	def get_context_data(self, **kwargs):
		context = super().get_context_data()
		context['ruta'] = self.kwargs['ruta']
		return context

class ClienteEliminar(LoginRequiredMixin, DeleteView):
	model = ClienteRuta
	success_url = reverse_lazy("clientes:listar_clientes")

	def get(self, request, *args, **kargs):
		objeto = ClienteRuta.objects.get(pk=self.kwargs['pk'])
		ruta = objeto.ruta.pk
		objeto.delete()
		return redirect("clientes:listar_clientes", id_ruta=ruta)
# Create your views here.

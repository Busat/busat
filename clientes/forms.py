from django import forms
from clientes.models import Cliente
from rutas.models import Ruta

class ClienteForm(forms.ModelForm):
	ruta = forms.CharField(
        widget=forms.TextInput(attrs={'class':'form-control', 'type':'hidden',}),
    )
	class Meta:
		model = Cliente
		fields = [ 'nombre','apellido','alias','direccion','celular','cpf']
		labels = {
			'nombre':'Nombre del Cliente',
			'apellido':'Apellido del Cliente',
			'alias':'Alias (Apodo)',
			'direccion':'Direccion (Recidencia)',
			'celular':'Celular (Contacto)',
			'cpf':'cpf (Documento)',
		}
		widgets = {
			'nombre': forms.TextInput(attrs = {'class':'form-control', }),
			'apellido': forms.TextInput(attrs = {'class':'form-control', }),
			'alias': forms.TextInput(attrs = {'class':'form-control', }),
			'direccion': forms.TextInput(attrs = {'class':'form-control', }),
			'celular': forms.TextInput(attrs = {'class':'form-control', 'type':'number'}),
			'cpf': forms.TextInput(attrs = {'class':'form-control', 'type':'number'}),
		}
		
class ClienteEditarForm(forms.ModelForm):
	class Meta:
		model = Cliente
		fields = [ 'nombre','apellido','alias','direccion','celular','cpf']
		exclude = ['ruta']
		labels = {
			'nombre':'Nombre del Cliente',
			'apellido':'Apellido del Cliente',
			'alias':'Alias (Apodo)',
			'direccion':'Direccion (Recidencia)',
			'celular':'Celular (Contacto)',
			'cpf':'cpf (Documento)',
		}
		widgets = {
			'nombre': forms.TextInput(attrs = {'class':'form-control', }),
			'apellido': forms.TextInput(attrs = {'class':'form-control', }),
			'alias': forms.TextInput(attrs = {'class':'form-control', }),
			'direccion': forms.TextInput(attrs = {'class':'form-control', }),
			'celular': forms.TextInput(attrs = {'class':'form-control', 'type':'number'}),
			'cpf': forms.TextInput(attrs = {'class':'form-control', 'type':'number'}),
		}
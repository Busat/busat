from django.urls import path

from clientes.views import ClienteCrear, ClienteListar, ClienteActualizar, ClienteEliminar, ClienteSinVentaActiva
from clientes.ajax import editar_estado
app_name = "clientes"

urlpatterns = [
	path('listar/<int:id_ruta>/', ClienteListar.as_view(), name = "listar_clientes"),
	path('listar/<int:id_ruta>/sin-venta-activa/', ClienteSinVentaActiva.as_view(), name = "listar_clientes_sin_venta_activa"),
	path('crear/', ClienteCrear.as_view(), name = "crear_cliente"),
	path('editar/<int:pk>/<int:ruta>/',ClienteActualizar.as_view(), name = "editar_cliente"),
	path('eliminar/<int:pk>/', ClienteEliminar.as_view(), name = "eliminar_cliente"),

	#Ajax
	path('estado/', editar_estado, name="editar_estado")
]	
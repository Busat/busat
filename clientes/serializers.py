from rest_framework.serializers import ModelSerializer, BooleanField

from clientes.models import Cliente, ClienteRuta

class ClienteSerializer(ModelSerializer):
    class Meta:
        model = Cliente
        fields = '__all__'

class ClienteRutaSerializer(ModelSerializer):
    cliente = ClienteSerializer()
    venta_activa = BooleanField(source = 'ventas_activas')

    class Meta:
        model = ClienteRuta
        fields = ('cliente', 'id', 'venta_activa')

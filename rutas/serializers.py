from rest_framework.serializers import ModelSerializer

from rutas.models import Ruta, Comunidad, IMEI


class RutaSerializer(ModelSerializer):
    class Meta:
        model=Ruta
        fields='__all__'

class ImeiSerializer(ModelSerializer):
    class Meta:
        model=IMEI
        fields='__all__'

class ComunidadSerializer(ModelSerializer):
    class Meta:
        model=Comunidad
        fields='__all__'
from rutas.views import Base, RutaListar, RutaCrear, RutaEditar, ImeiListar, ImeiCrear, ImeiEditar, RutaEliminar, ImeiEliminar
from django.urls import path
from rutas.ajax import cambiar_estado, cierre_ruta

app_name = "rutas"

urlpatterns = [
    path('base/', Base.as_view(), name="base"),
    path('listar/', RutaListar.as_view(), name = "listar_rutas"),
    path('crear/', RutaCrear.as_view(), name = "crear_ruta"),
    path('editar/<int:pk>/', RutaEditar.as_view(), name = "editar_ruta"),
    path('listar/imei/', ImeiListar.as_view(), name = "listar_imei"),
    path('crear/imei/', ImeiCrear.as_view(), name = "crear_imei"),
    path('editar/imei/<int:pk>/', ImeiEditar.as_view(), name = "editar_imei"),
    path('eliminar/<int:pk>/',RutaEliminar.as_view(), name='eliminar'),
    path('imei/eliminar/<int:pk>/',ImeiEliminar.as_view(), name='eliminar_imei'),

    #AJAX
    path('estado/', cambiar_estado, name = "cambiar_estado_ruta"),
    path('cierre/ruta/', cierre_ruta, name = "pagos_cierre_ruta"),

]
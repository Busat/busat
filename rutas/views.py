from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.views.generic import TemplateView
from django.shortcuts import render, redirect, HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic.edit import FormView, UpdateView
from django.views.generic.edit import CreateView, DeleteView
from django.views.generic import ListView
from rutas.models import Ruta, IMEI
from rutas.forms import RutaForm, ImeiForm

class Base(LoginRequiredMixin, TemplateView):
    template_name = "base/base.html"

class RutaCrear(LoginRequiredMixin, CreateView):
	model = Ruta
	form_class = RutaForm
	template_name = "bt_rutas/editar_ruta.html"
	success_url = reverse_lazy('rutas:listar_rutas')

class RutaEditar(LoginRequiredMixin, UpdateView):
	model = Ruta
	form_class = RutaForm
	template_name = "bt_rutas/editar_ruta.html"
	success_url = reverse_lazy('rutas:listar_rutas')

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		objeto = Ruta.objects.get(pk=self.kwargs['pk'])
		form = RutaForm(instance=objeto, initial={'user':self.request.user.comunidad})
		context['form'] = form
		return context

class RutaListar(LoginRequiredMixin, ListView):
	model = Ruta
	template_name = "bt_rutas/listar_rutas.html"

	def get_queryset(self):
		queryset = super().get_queryset()
		if self.request.user.has_perm("usuarios.is_secretarias"):
			return queryset.filter(pk__in=[contenido.pk for contenido in self.request.user.rutas.all()]).order_by("pk")
		return queryset.filter(imei__comunidad=self.request.user.comunidad.pk).order_by("pk")

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		form = RutaForm( initial={'user':self.request.user.comunidad})
		context['form'] = form
		return context

class ImeiCrear(LoginRequiredMixin, CreateView):
	model = IMEI
	form_class = ImeiForm
	template_name = "bt_rutas/editar_imei.html"
	success_url = reverse_lazy('rutas:listar_imei')

class ImeiEditar(LoginRequiredMixin, UpdateView):
	model = IMEI
	form_class = ImeiForm
	template_name = "bt_rutas/editar_imei.html"
	success_url = reverse_lazy('rutas:listar_imei')

class ImeiListar(LoginRequiredMixin, ListView):
	model = IMEI
	template_name = "bt_rutas/listar_imei.html"

	def get_queryset(self):
		queryset = super().get_queryset()
		if self.request.user.has_perm("usuarios.is_secretarias"):
			return queryset.filter(pk__in=[contenido.pk for contenido in self.request.user.rutas.all()])
		return queryset.filter(comunidad=self.request.user.comunidad.pk)

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		form = ImeiForm({'user':self.request.user.comunidad})
		context['form'] = form
		return context

class RutaEliminar(LoginRequiredMixin, DeleteView):
	model = Ruta
	success_url = reverse_lazy("rutas:listar_rutas")

	def get(self, request, *args, **kargs):
		objeto = Ruta.objects.get(pk=self.kwargs['pk'])
		objeto.delete()
		return redirect("rutas:listar_rutas")

class ImeiEliminar(LoginRequiredMixin, DeleteView):
	model = IMEI
	success_url = reverse_lazy("rutas:listar_imei")

	def get(self, request, *args, **kargs):
		objeto = IMEI.objects.get(pk=self.kwargs['pk'])
		objeto.delete()
		return redirect("rutas:listar_imei")
# Create your views here.

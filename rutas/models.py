from django.db import models
from django.urls import reverse
from django_extensions.db.models import TimeStampedModel

class Comunidad(TimeStampedModel):
	nombre = models.CharField(max_length=100, blank=True, null=True, unique=True)
	estado = models.BooleanField(default=True)
	def __str__(self):
		return self.nombre

	#Contador de rutas por comunidad
	def rutasPorComunidad(self):
		return Ruta.objects.filter(imei__comunidad=self.pk).count()

class IMEI(TimeStampedModel):
	codigo = models.CharField(max_length=40, unique=True)
	password = models.CharField(max_length=10, blank=True, null=True)
	comunidad = models.ForeignKey(Comunidad, on_delete = models.CASCADE)
	supervisor = models.BooleanField(default=False, blank=True)
	activo = models.BooleanField(default=True, blank=True)
	def __str__(self):
		return "%s"%self.codigo

class Ruta(TimeStampedModel):
	nombre = models.CharField(max_length=50, blank=True, null=True)
	imei = models.OneToOneField(IMEI, on_delete = models.CASCADE, unique=True)

	def __str__(self):
		return "%d - %s"%(self.pk, self.nombre)

	def ultima_caja(self):
		from caja.models import Caja
		caja = Caja.objects.filter(ruta=self.pk).order_by("-pk").first()
		if caja:
			return caja
		else:
			return False

	def estado(self):
		caja=self.ultima_caja()
		if caja:
			return caja.abierta
		else:
			return False

	def get_ultima_caja_url(self):
		caja = self.ultima_caja()
		if caja:
			return reverse("caja:resumen_caja", kwargs={'caja':caja.pk})
		else:
			return reverse("rutas:listar_rutas")
# Create your models here.

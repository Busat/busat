# Generated by Django 2.1 on 2019-02-28 21:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rutas', '0002_auto_20190228_1630'),
    ]

    operations = [
        migrations.AlterField(
            model_name='imei',
            name='activo',
            field=models.BooleanField(blank=True, default=True),
        ),
        migrations.AlterField(
            model_name='imei',
            name='supervisor',
            field=models.BooleanField(blank=True, default=False),
        ),
    ]

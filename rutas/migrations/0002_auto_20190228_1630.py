# Generated by Django 2.1 on 2019-02-28 16:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rutas', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='imei',
            name='activo',
            field=models.BooleanField(blank=True, default=True, null=True),
        ),
        migrations.AddField(
            model_name='imei',
            name='supervisor',
            field=models.BooleanField(blank=True, default=False, null=True),
        ),
    ]

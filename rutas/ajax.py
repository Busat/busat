from django.http import JsonResponse
from caja.models import Caja
from rutas.models import Ruta
from ventas.models import Venta, VentaNoCobrado
from pagos.models import Pago, TipoPago

def cambiar_estado(request):
	ruta = Ruta.objects.get(pk=int(request.GET.get('id_ruta')))
	caja = Caja.objects.filter(ruta = int(request.GET.get('id_ruta'))).order_by("-pk").first()
	if caja:
		if caja.abierta:
			validar = caja.ventanocobrado_set.all().first()
			if validar:
				validar = validar.venta.all().count()
			else:
				validar = 0
			if validar==0:
				caja.abierta = False
				caja.save()
			else:
				res = caja.ventanocobrado_set.all().first()
				return JsonResponse({'opc2':"Tiene %d ventas por cobrar hoy, estas seguro que quieres cerrar ruta?"%res.venta.all().count()})
		else:			
			instance = Caja(ruta=ruta, valor_inicial=caja.valor_final)
			instance.save()
			ventas = Venta.objects.filter(cliente_ruta__ruta__pk=ruta.pk, no_cobrado=True)
			instance2 = VentaNoCobrado(caja=instance)
			instance2.save()
			for data in ventas:
				instance2.venta.add(data)
	else:
		instance = Caja(ruta=ruta)
		instance.save()
	return JsonResponse({'opc':True})

def cierre_ruta(request):
	ruta = Ruta.objects.get(pk=int(request.GET.get('id_ruta')))
	caja = Caja.objects.filter(ruta = int(request.GET.get('id_ruta'))).order_by("-pk").first()
	tipo_pago = TipoPago.objects.get(descripcion__contains="SIN PAGO")
	print("Tiene caja: ",caja, " Tiene Ruta: ", ruta)
	if caja:
		caja_ventas = caja.ventanocobrado_set.all().first()
		ventas = caja_ventas.venta.all()
		for contenido in ventas:
			print("Entra")
			instance = Pago(tipo_pago=tipo_pago, venta=contenido, caja=caja)
			instance.save()
			caja_ventas.venta.remove(contenido)
		caja.abierta = False
		caja.save()
		return JsonResponse({'opc':True})
	else:
		return JsonResponse({'res':True})


from django import forms
from rutas.models import Ruta, IMEI, Comunidad

class RutaForm(forms.ModelForm):
	class Meta:
		model = Ruta
		fields = ['nombre','imei',]
		labels = {
		'nombre':'Nombre de Ruta',
		'imei': 'IMEI del dispositivo',
		}
		widgets  = {
		'nombre':forms.TextInput(attrs = {'class':'form-control'}),
		'imei': forms.Select(attrs = {'class': 'form-control'}),
		}
	def __init__(self, *args, **kwargs):
		super(RutaForm, self).__init__(*args, **kwargs)
		if 'initial' in kwargs:
			if 'user' in kwargs['initial']:
				self.fields['imei'].queryset = IMEI.objects.filter(comunidad=kwargs['initial']['user'].pk)

class ImeiForm(forms.ModelForm):
	class Meta: 
		model = IMEI
		fields = ['codigo','password','comunidad',]
		labels = {
		'codigo':'IMEI del dispositivo',
		'password':'Contraseña',
		'comunidad':'Comunidad',
		}
		widgets = {
		'codigo': forms.TextInput(attrs={'class':'form-control'}),
		'password':forms.TextInput(attrs={'class':'form-control'}),
		'comunidad':forms.TextInput(attrs={'class': 'form-control','style':'visibility:hidden'}),
		}


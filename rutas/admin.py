from django.contrib import admin
from rutas.models import Ruta, IMEI

#Modelo que muestra es listado de Rutas en el admin
class RutaAdmin(admin.ModelAdmin):
	list_display= ['pk', 'nombre','imei']

#Modelo que muestra el listado de IMEI en el admin
class IMEIAdmin(admin.ModelAdmin):
	list_display=['pk', 'codigo','password','comunidad',]

admin.site.register(Ruta, RutaAdmin)
admin.site.register(IMEI, IMEIAdmin)



# Register your models here.

from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

# Views
from api.views import *

urlpatterns = [
    path('login/<slug:imei>/', Login.as_view()),
    path('sincronizacion/<int:ruta>/', Sincronizacion.as_view()),
    path('cierre/caja/<int:caja>/', CierreCaja.as_view()),
    path('transacciones/<slug:imei>/', Transacciones.as_view()),
    path('inversiones/<slug:imei>/', Inversiones.as_view()),
    path('rutas/', Rutas.as_view()),

    path('sincronizacion/movimientos/', SincronizacionMovimientos.as_view()),
    path('sincronizacion/clientes/editados/', SincronizacionClientesEditados.as_view()),
    path('sincronizacion/pagos/', SincronizacionPagos.as_view()),
    path('sincronizacion/clientes/', SincronizacionClientes.as_view()),
    path('sincronizacion/ventas/', SincronizacionVentas.as_view()),
]

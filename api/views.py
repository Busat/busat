# from django.core.exceptions import MultipleObjectsReturned
from django.db.models import Q
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
    # HTTP_200_OK
    # HTTP_201_CREATED
    # HTTP_404_NOT_FOUND
    # HTTP_400_BAD_REQUEST

# Models
from caja.models import Caja
from clientes.models import ClienteRuta, Cliente
from movimientos.models import TipoMovimiento, Opcion, Movimiento
from pagos.models import TipoPago, Motivo, Pago
from retiros.models import Transaccion, Inversion
from rutas.models import IMEI, Ruta
from ventas.models import Venta, VentaNoCobrado
from usuarios.models import User

# Serializers
from caja.serializers import CajaSerializer
from clientes.serializers import ClienteRutaSerializer, ClienteSerializer
from movimientos.serializers import TipoMovimientoSerializer, OpcionSerializer, MovimientoSerializer
from pagos.serializers import TipoPagoSerializer, MotivoSerializer, PagoSerializer, PagoInformacionSerializer
from retiros.serializers import TransaccionSerializer, InversionSerializer, CrearTransaccionSerializer, CrearInversionSerializer
from rutas.serializers import RutaSerializer
from ventas.serializers import VentaSerializer, CrearVentaSerializer

# Views.
class Login(APIView):

    def get(self, request, imei, format = None):
        try:
            datos_imei = IMEI.objects.get(codigo = imei)
            lista_tipo_movimientos = TipoMovimiento.objects.all()
            lista_tipo_movimientos = TipoMovimientoSerializer(lista_tipo_movimientos, many = True)
            lista_opciones = Opcion.objects.all()
            lista_opciones = OpcionSerializer(lista_opciones, many = True)
            lista_tipo_pagos = TipoPago.objects.all()
            lista_tipo_pagos = TipoPagoSerializer(lista_tipo_pagos, many = True)
            lista_motivos = Motivo.objects.all()
            lista_motivos = MotivoSerializer(lista_motivos, many = True)
            context = {
                'activo': True,
                'id_imei': datos_imei.id,
                'supervisor': datos_imei.supervisor,
                'activo': datos_imei.activo,
                'id_ruta': datos_imei.ruta.pk,
                'password': datos_imei.password,
                'lista_tipo_movimientos': lista_tipo_movimientos.data,
                'lista_opciones': lista_opciones.data,
                'lista_tipo_pagos': lista_tipo_pagos.data,
                'lista_motivos': lista_motivos.data,
            }
            return Response(context, status = status.HTTP_200_OK)
        except IMEI.DoesNotExist:
            return Response({
                'activo': False,
                'mensaje': 'El imei no esta registrado en la plataforma.'
            }, status = status.HTTP_200_OK)

class Sincronizacion(APIView):

    def get(self, request, ruta, format = None):
        try:
            lista_clientes = ClienteRuta.objects.filter(ruta__id = ruta, activo = True)
            lista_ventas = Venta.objects.filter(no_cobrado = True, cliente_ruta__in = lista_clientes)
            datos_caja = Caja.objects.get(ruta__id = ruta, abierta = True)
            # lista_pagos = Pago.objects.filter(caja = datos_caja)
            lista_pagos = Pago.objects.filter(Q(venta__in = lista_ventas) | Q(caja = datos_caja))
            lista_pagos = lista_pagos.filter(tipo_pago=1)
            lista_movimientos = Movimiento.objects.filter(caja = datos_caja)
            lista_clientes = ClienteRutaSerializer(lista_clientes, many = True)
            lista_ventas = VentaSerializer(lista_ventas, many = True)
            lista_pagos = PagoInformacionSerializer(lista_pagos, many = True)
            lista_movimientos = MovimientoSerializer(lista_movimientos, many = True)
            datos_caja = CajaSerializer(datos_caja)
            context = {
                'activo': True,
                'lista_clientes': lista_clientes.data,
                'lista_ventas': lista_ventas.data,
                'lista_pagos': lista_pagos.data,
                'lista_movimientos': lista_movimientos.data,
                'caja': datos_caja.data,
            }
            return Response(context, status = status.HTTP_200_OK)
        except Caja.DoesNotExist:
            return Response({
                'activo': False,
                'mensaje': 'La ruta no tiene una caja abierta.'
            }, status = status.HTTP_200_OK)
        except Caja.MultipleObjectsReturned:
            return Response({
                'activo': False,
                'mensaje': 'La ruta tiene más de una caja abierta.'
            }, status = status.HTTP_200_OK)

    def post(self, request, ruta, format = None):
        '''
        Por cuerpo:
            - lista pagos           - lista pagos ventas existentes
            - lista clientes        - lista movimientos
            - lista ventas          - lista ventas clientes existentes
            - caja                  - lista clientes editados
        '''
        # Lista de respuesta
        lista_movimientos = []
        lista_clientes = []
        lista_clientes_editados = []
        lista_ventas = []
        lista_pagos = []
        lista_pagos_nuevos = []
        lista_ventas_nuevas = []
        lista_clientes = []

        # Informacion ruta
        ruta_datos = Ruta.objects.get(id = ruta)

        # Se almacenan los movimientos nuevos
        for movimiento in request.data['lista_movimientos']:
            movimiento_nuevo = MovimientoSerializer(data = movimiento)
            if movimiento_nuevo.is_valid():
                movimiento_nuevo.save()
                data_movimiento = movimiento_nuevo.data
                data_movimiento['id_movil'] = movimiento.pop('id')
                lista_movimientos.append(data_movimiento)
            else:
                lista_ventas.extend(lista_ventas_nuevas)
                lista_pagos.extend(lista_pagos_nuevos)

                context = {
                    'lista_clientes': lista_clientes,
                    'lista_clientes_editados': lista_clientes_editados,
                    'lista_ventas': lista_ventas,
                    'lista_pagos': lista_pagos,
                    'lista_movimientos': lista_movimientos,
                    'estado': False,
                    'mensaje': 'Error al crear movimientos.'
                }
                return Response(context, status = status.HTTP_200_OK)

        # Se almacenan los pagos de las ventas ya existentes en la web
        for pago in request.data['lista_pagos_ventas_existentes']:
            pago_nuevo = PagoSerializer(data = pago)
            if pago_nuevo.is_valid():
                pago_nuevo.save()
                data_pago = pago_nuevo.data
                data_pago['id_movil'] = pago.get('id')
                lista_pagos.append(data_pago)
            else:
                lista_ventas.extend(lista_ventas_nuevas)
                lista_pagos.extend(lista_pagos_nuevos)

                context = {
                    'lista_clientes': lista_clientes,
                    'lista_clientes_editados': lista_clientes_editados,
                    'lista_ventas': lista_ventas,
                    'lista_pagos': lista_pagos,
                    'lista_movimientos': lista_movimientos,
                    'estado': False,
                    'mensaje': 'Error al crear pagos de ventas existentes.'
                }
                return Response(context, status = status.HTTP_200_OK)

        # Se actualizan los clientes editados
        for cliente in request.data['lista_clientes_editados']:
            id_cliente_movil = cliente.get('id')
            datos_cliente = ClienteRuta.objects.get(id = cliente.pop('id_web'))
            cliente_serializer = ClienteSerializer(datos_cliente.cliente, data = cliente, partial = True)
            if cliente_serializer.is_valid():
                cliente_serializer.save()
                cliente_ruta = ClienteRutaSerializer(datos_cliente)
                lista_clientes_editados.append(cliente_ruta.data)
            else:
                lista_ventas.extend(lista_ventas_nuevas)
                lista_pagos.extend(lista_pagos_nuevos)

                context = {
                    'lista_clientes': lista_clientes,
                    'lista_clientes_editados': lista_clientes_editados,
                    'lista_ventas': lista_ventas,
                    'lista_pagos': lista_pagos,
                    'lista_movimientos': lista_movimientos,
                    'estado': False,
                    'mensaje': 'Error al actualizar información de clientes editados.'
                }
                return Response(context, status = status.HTTP_200_OK)

        # Remover ventas cobradas
        try:
            ventas_no_cobrados = VentaNoCobrado.objects.get(caja = request.data['id_caja'])
            for pago in lista_pagos:
                info_pago = Pago.objects.get(id = pago['id'])
                if info_pago.venta.saldo() <= 0:
                    info_pago.venta.no_cobrado = False
                    info_pago.venta.save()
                ventas_no_cobrados.venta.remove(info_pago.venta)
        except VentaNoCobrado.DoesNotExist:
            pass

        # Se almacenan las ventas nuevas con cliente web
        for venta in request.data['lista_ventas_clientes_existentes']:
            bandera_crear_venta = False
            id_venta_movil = venta.get('id')
            venta_nueva = CrearVentaSerializer(data = venta)
            if venta_nueva.is_valid():
                venta_nueva.save()
                venta_nueva = Venta.objects.get(id = venta_nueva.data['id'])

                if len(request.data['lista_pagos']) == 0:
                    venta_nueva = VentaSerializer(venta_nueva)
                    data_venta = venta_nueva.data
                    data_venta['id_movil'] = id_venta_movil
                    lista_ventas.append(data_venta)
                    bandera_crear_venta = True

                # Se almacena el pago de la venta
                for pago in request.data['lista_pagos']:
                    if pago['venta_movil'] == id_venta_movil:
                        pago['venta'] = venta_nueva.id
                        pago_nuevo = PagoSerializer(data = pago)
                        if pago_nuevo.is_valid():
                            pago_nuevo.save()
                            data_pago = pago_nuevo.data
                            data_pago['id_movil'] = pago.get('id')
                            lista_pagos_nuevos.append(data_pago)

                            venta_nueva = VentaSerializer(venta_nueva)
                            data_venta = venta_nueva.data
                            data_venta['id_movil'] = id_venta_movil
                            lista_ventas.append(data_venta)
                            bandera_crear_venta = True
                            break
                        else:
                            lista_ventas.extend(lista_ventas_nuevas)
                            lista_pagos.extend(lista_pagos_nuevos)

                            context = {
                                'lista_clientes': lista_clientes,
                                'lista_clientes_editados': lista_clientes_editados,
                                'lista_ventas': lista_ventas,
                                'lista_pagos': lista_pagos,
                                'lista_movimientos': lista_movimientos,
                                'estado': False,
                                'mensaje': 'Error al crear pago.'
                            }
                            return Response(context, status = status.HTTP_200_OK)

                if not bandera_crear_venta:
                    venta_nueva = VentaSerializer(venta_nueva)
                    data_venta = venta_nueva.data
                    data_venta['id_movil'] = id_venta_movil
                    lista_ventas.append(data_venta)

            else:
                lista_ventas.extend(lista_ventas_nuevas)
                lista_pagos.extend(lista_pagos_nuevos)

                context = {
                    'lista_clientes': lista_clientes,
                    'lista_clientes_editados': lista_clientes_editados,
                    'lista_ventas': lista_ventas,
                    'lista_pagos': lista_pagos,
                    'lista_movimientos': lista_movimientos,
                    'estado': False,
                    'mensaje': 'Error al crear venta.'
                }
                return Response(context, status = status.HTTP_200_OK)

        # Se almacen los clientes nuevos
        for cliente in request.data['lista_clientes']:
            id_cliente_movil = cliente.get('id')
            cliente_antiguo = None
            bandera_crear_cliente = False
            bandera_crear_venta = False

            try:
                cliente_antiguo = Cliente.objects.get(cpf = cliente.get('cpf'))
                ClienteRuta.objects.create(cliente = cliente_antiguo, ruta = ruta_datos)
            except Cliente.DoesNotExist:
                cliente_nuevo = ClienteSerializer(data = cliente)
                if cliente_nuevo.is_valid():
                    cliente_nuevo.save()
                    cliente_antiguo = Cliente.objects.get(cpf = cliente.get('cpf'))
                    ClienteRuta.objects.create(cliente = cliente_antiguo, ruta = ruta_datos)
                else:
                    lista_ventas.extend(lista_ventas_nuevas)
                    lista_pagos.extend(lista_pagos_nuevos)

                    context = {
                        'lista_clientes': lista_clientes,
                        'lista_clientes_editados': lista_clientes_editados,
                        'lista_ventas': lista_ventas,
                        'lista_pagos': lista_pagos,
                        'lista_movimientos': lista_movimientos,
                        'estado': False,
                        'mensaje': 'Error al crear cliente.'
                    }
                    return Response(context, status = status.HTTP_200_OK)

            cliente_antiguo_opcional = ClienteRuta.objects.get(cliente = cliente_antiguo, ruta = ruta_datos)

            if len(request.data['lista_ventas']) == 0:
                cliente_antiguo = ClienteRuta.objects.get(cliente = cliente_antiguo, ruta = ruta_datos)
                cliente_antiguo = ClienteRutaSerializer(cliente_antiguo)
                data_cliente = cliente_antiguo.data
                data_cliente['id_movil'] = id_cliente_movil
                lista_clientes.append(data_cliente)
                bandera_crear_cliente = True

            # Se almacena la venta del cliente
            for venta in request.data['lista_ventas']:
                if venta['cliente_movil'] == id_cliente_movil:
                    id_venta_movil = venta.get('id')
                    venta['cliente_ruta'] = cliente_antiguo_opcional.id
                    venta_nueva = CrearVentaSerializer(data = venta)
                    if venta_nueva.is_valid():
                        venta_nueva.save()
                        venta_nueva = Venta.objects.get(id = venta_nueva.data['id'])

                        cliente_antiguo = ClienteRuta.objects.get(cliente = cliente_antiguo, ruta = ruta_datos)
                        cliente_antiguo = ClienteRutaSerializer(cliente_antiguo)
                        data_cliente = cliente_antiguo.data
                        data_cliente['id_movil'] = id_cliente_movil
                        lista_clientes.append(data_cliente)
                        bandera_crear_cliente = True

                        if len(request.data['lista_pagos']) == 0:
                            venta_nueva = VentaSerializer(venta_nueva)
                            data_venta = venta_nueva.data
                            data_venta['id_movil'] = id_venta_movil
                            lista_ventas_nuevas.append(data_venta)
                            bandera_crear_venta = True

                        # Se almacena el pago de la venta
                        for pago in request.data['lista_pagos']:
                            if pago['venta_movil'] == id_venta_movil:
                                pago['venta'] = venta_nueva.id
                                pago_nuevo = PagoSerializer(data = pago)
                                if pago_nuevo.is_valid():
                                    pago_nuevo.save()
                                    data_pago = pago_nuevo.data
                                    data_pago['id_movil'] = pago.get('id')
                                    lista_pagos_nuevos.append(data_pago)

                                    venta_nueva = VentaSerializer(venta_nueva)
                                    data_venta = venta_nueva.data
                                    data_venta['id_movil'] = id_venta_movil
                                    lista_ventas_nuevas.append(data_venta)
                                    bandera_crear_venta = True
                                    break
                                else:
                                    lista_ventas.extend(lista_ventas_nuevas)
                                    lista_pagos.extend(lista_pagos_nuevos)

                                    context = {
                                        'lista_clientes': lista_clientes,
                                        'lista_clientes_editados': lista_clientes_editados,
                                        'lista_ventas': lista_ventas,
                                        'lista_pagos': lista_pagos,
                                        'lista_movimientos': lista_movimientos,
                                        'estado': False,
                                        'mensaje': 'Error al crear pago.'
                                    }
                                    return Response(context, status = status.HTTP_200_OK)

                        if not bandera_crear_venta:
                            venta_nueva = VentaSerializer(venta_nueva)
                            data_venta = venta_nueva.data
                            data_venta['id_movil'] = id_venta_movil
                            lista_ventas_nuevas.append(data_venta)

                        break

                    else:
                        lista_ventas.extend(lista_ventas_nuevas)
                        lista_pagos.extend(lista_pagos_nuevos)

                        context = {
                            'lista_clientes': lista_clientes,
                            'lista_clientes_editados': lista_clientes_editados,
                            'lista_ventas': lista_ventas,
                            'lista_pagos': lista_pagos,
                            'lista_movimientos': lista_movimientos,
                            'estado': False,
                            'mensaje': 'Error al crear venta.'
                        }
                        return Response(context, status = status.HTTP_200_OK)

            if not bandera_crear_cliente:
                cliente_antiguo = ClienteRuta.objects.get(cliente = cliente_antiguo, ruta = ruta_datos)
                cliente_antiguo = ClienteRutaSerializer(cliente_antiguo)
                data_cliente = cliente_antiguo.data
                data_cliente['id_movil'] = id_cliente_movil
                lista_clientes.append(data_cliente)

        lista_ventas.extend(lista_ventas_nuevas)
        lista_pagos.extend(lista_pagos_nuevos)

        context = {
            'lista_clientes': lista_clientes,
            'lista_clientes_editados': lista_clientes_editados,
            'lista_ventas': lista_ventas,
            'lista_pagos': lista_pagos,
            'lista_movimientos': lista_movimientos,
            'estado': True,
        }
        return Response(context, status = status.HTTP_200_OK)

class CierreCaja(APIView):

    def get(self, request, caja, format = None):
        try:
            ventas_no_cobrados = VentaNoCobrado.objects.get(caja = caja)
            if ventas_no_cobrados.venta.count() != 0:
                return Response({ 'cierre_caja': False, 'mensaje': 'Faltan ventas por cobrar.' }, status = status.HTTP_200_OK)
            ventas_no_cobrados.caja.abierta = False
            ventas_no_cobrados.caja.save()
        except VentaNoCobrado.DoesNotExist:
            datos_caja = Caja.objects.get(id = caja)
            datos_caja.abierta = False
            datos_caja.save()
        return Response({ 'cierre_caja': True, 'mensaje': 'Caja cerrada.' }, status = status.HTTP_200_OK)

class Transacciones(APIView):

    def get(self, request, imei, format = None):
        datos_imei = IMEI.objects.get(codigo = imei)
        transacciones = Transaccion.objects.filter(usuario__comunidad = datos_imei.comunidad)
        transacciones = TransaccionSerializer(transacciones, many = True)
        return Response(transacciones.data, status = status.HTTP_200_OK)

    def post(self, request, imei, format = None):
        datos_imei = IMEI.objects.get(codigo = imei)
        users = User.objects.filter(comunidad = datos_imei.comunidad)
        user_comunidad = None
        for user in users:
            if user.admin_comunidad():
                user_comunidad = user
                break
        request.data['usuario'] = user_comunidad.id
        serializer = CrearTransaccionSerializer(data = request.data)
        serializer.is_valid(raise_exception = True)
        serializer.save()
        return Response(serializer.data, status = status.HTTP_201_CREATED)

class Inversiones(APIView):

    def get(self, request, imei, format = None):
        datos_imei = IMEI.objects.get(codigo = imei)
        inversiones = Inversion.objects.filter(usuario__comunidad = datos_imei.comunidad)
        inversiones = InversionSerializer(inversiones, many = True)
        return Response(inversiones.data, status = status.HTTP_200_OK)

    def post(self, request, imei, format = None):
        datos_imei = IMEI.objects.get(codigo = imei)
        users = User.objects.filter(comunidad = datos_imei.comunidad)
        user_comunidad = None
        for user in users:
            if user.admin_comunidad():
                user_comunidad = user
                break
        request.data['usuario'] = user_comunidad.id
        serializer = CrearInversionSerializer(data = request.data)
        serializer.is_valid(raise_exception = True)
        serializer.save()
        return Response(serializer.data, status = status.HTTP_201_CREATED)

class Rutas(APIView):

    def get(self, request, format = None):
        rutas = Ruta.objects.all()
        rutas = RutaSerializer(rutas, many = True)
        return Response(rutas.data, status = status.HTTP_200_OK)

class SincronizacionMovimientos(APIView):

    def post(self, request, format = None):
        # Se almacenan los movimientos nuevos
        lista_movimientos = []
        for movimiento in request.data['lista_movimientos']:
            movimiento_nuevo = MovimientoSerializer(data = movimiento)
            if movimiento_nuevo.is_valid():
                movimiento_nuevo.save()
                data_movimiento = movimiento_nuevo.data
                data_movimiento['id_movil'] = movimiento.pop('id')
                lista_movimientos.append(data_movimiento)
            else:
                context = {
                    'lista_movimientos': lista_movimientos,
                    'estado': False,
                    'mensaje': 'Error al crear movimientos.'
                }
                return Response(context, status = status.HTTP_200_OK)

        context = {
            'lista_movimientos': lista_movimientos,
            'estado': True,
        }
        return Response(context, status = status.HTTP_200_OK)

class SincronizacionClientesEditados(APIView):

    def post(self, request, format = None):
        lista_clientes_editados = []
        for cliente in request.data['lista_clientes_editados']:
            id_cliente_movil = cliente.get('id')
            datos_cliente = ClienteRuta.objects.get(id = cliente.pop('id_web'))
            cliente_serializer = ClienteSerializer(datos_cliente.cliente, data = cliente, partial = True)
            if cliente_serializer.is_valid():
                cliente_serializer.save()
                cliente_ruta = ClienteRutaSerializer(datos_cliente)
                lista_clientes_editados.append(cliente_ruta.data)
            else:
                context = {
                    'lista_clientes_editados': lista_clientes_editados,
                    'estado': False,
                    'mensaje': 'Error al actualizar información de clientes editados.'
                }
                return Response(context, status = status.HTTP_200_OK)

        context = {
            'lista_clientes_editados': lista_clientes_editados,
            'estado': True,
        }
        return Response(context, status = status.HTTP_200_OK)

class SincronizacionPagos(APIView):

    def post(self, request, format = None):
        lista_pagos = []
        # Remover ventas cobradas
        try:
            ventas_no_cobrados = VentaNoCobrado.objects.get(caja = request.data['id_caja'])
            # Se almacenan los pagos de las ventas ya existentes en la web
            for pago in request.data['lista_pagos_ventas_existentes']:
                pago_nuevo = PagoSerializer(data = pago)
                if pago_nuevo.is_valid():
                    pago_nuevo.save()
                    data_pago = pago_nuevo.data
                    data_pago['id_movil'] = pago.get('id')
                    info_pago = Pago.objects.get(id = data_pago['id'])
                    if info_pago.venta.saldo() <= 0:
                        info_pago.venta.no_cobrado = False
                        info_pago.venta.save()
                    lista_pagos.append(data_pago)
                    ventas_no_cobrados.venta.remove(info_pago.venta)
                else:
                    context = {
                        'lista_pagos': lista_pagos,
                        'estado': False,
                        'mensaje': 'Error al crear pagos de ventas existentes.'
                    }
                    return Response(context, status = status.HTTP_200_OK)
        except VentaNoCobrado.DoesNotExist:
            for pago in request.data['lista_pagos_ventas_existentes']:
                pago_nuevo = PagoSerializer(data = pago)
                if pago_nuevo.is_valid():
                    pago_nuevo.save()
                    data_pago = pago_nuevo.data
                    data_pago['id_movil'] = pago.get('id')
                    info_pago = Pago.objects.get(id = data_pago['id'])
                    if info_pago.venta.saldo() <= 0:
                        info_pago.venta.no_cobrado = False
                        info_pago.venta.save()
                    lista_pagos.append(data_pago)
                else:
                    context = {
                        'lista_pagos': lista_pagos,
                        'estado': False,
                        'mensaje': 'Error al crear pagos de ventas existentes.'
                    }
                    return Response(context, status = status.HTTP_200_OK)

        context = {
            'lista_pagos': lista_pagos,
            'estado': True,
        }
        return Response(context, status = status.HTTP_200_OK)

class SincronizacionClientes(APIView):

    def post(self, request, format = None):
        lista_clientes = []
        ruta_datos = Ruta.objects.get(id = request.data['ruta'])
        # Se almacen los clientes nuevos
        for cliente in request.data['lista_clientes']:
            id_cliente_movil = cliente.get('id')

            try:
                cliente_antiguo = Cliente.objects.get(cpf = cliente.get('cpf'))
                ClienteRuta.objects.create(cliente = cliente_antiguo, ruta = ruta_datos)
                cliente_antiguo = ClienteRuta.objects.get(cliente = cliente_antiguo, ruta = ruta_datos)
                cliente_antiguo = ClienteRutaSerializer(cliente_antiguo)
                data_cliente = cliente_antiguo.data
                data_cliente['id_movil'] = id_cliente_movil
                lista_clientes.append(data_cliente)
            except Cliente.DoesNotExist:
                cliente_nuevo = ClienteSerializer(data = cliente)
                if cliente_nuevo.is_valid():
                    cliente_nuevo.save()
                    cliente_antiguo = Cliente.objects.get(cpf = cliente.get('cpf'))
                    ClienteRuta.objects.create(cliente = cliente_antiguo, ruta = ruta_datos)

                    cliente_antiguo = ClienteRuta.objects.get(cliente = cliente_antiguo, ruta = ruta_datos)
                    cliente_antiguo = ClienteRutaSerializer(cliente_antiguo)
                    data_cliente = cliente_antiguo.data
                    data_cliente['id_movil'] = id_cliente_movil
                    lista_clientes.append(data_cliente)
                else:
                    context = {
                        'lista_clientes': lista_clientes,
                        'estado': False,
                        'mensaje': 'Error al crear cliente.'
                    }
                    return Response(context, status = status.HTTP_200_OK)

        context = {
            'lista_clientes': lista_clientes,
            'estado': True,
        }
        return Response(context, status = status.HTTP_200_OK)

class SincronizacionVentas(APIView):

    def post(self, request, format = None):
        # Se almacenan las ventas nuevas con cliente web
        lista_ventas = []
        for venta in request.data['lista_ventas']:
            id_venta_movil = venta.get('id')
            venta_nueva = CrearVentaSerializer(data = venta)
            if venta_nueva.is_valid():
                venta_nueva.save()
                venta_nueva = Venta.objects.get(id = venta_nueva.data['id'])

                venta_nueva = VentaSerializer(venta_nueva)
                data_venta = venta_nueva.data
                data_venta['id_movil'] = id_venta_movil
                lista_ventas.append(data_venta)
            else:
                context = {
                    'lista_ventas': lista_ventas,
                    'estado': False,
                    'mensaje': 'Error al crear venta.'
                }
                return Response(context, status = status.HTTP_200_OK)

        context = {
            'lista_ventas': lista_ventas,
            'estado': True,
        }
        return Response(context, status = status.HTTP_200_OK)

from django.urls import path
from caja.views import CajaResumen, CajaPeriodoView

app_name = "caja"

urlpatterns = [
	path('resumen/<int:caja>',CajaResumen.as_view(), name = "resumen_caja" ),
	path('periodo/<int:ruta>', CajaPeriodoView.as_view(), name= "resumen_periodo_caja"),
]
from rest_framework.serializers import ModelSerializer

from caja.models import Caja

class CajaSerializer(ModelSerializer):
    class Meta:
        model = Caja
        fields = '__all__'

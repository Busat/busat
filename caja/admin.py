from django.contrib import admin
from caja.models import Caja

#Lista todas las cajas en el admin de django
class CajaAdmin(admin.ModelAdmin):
	list_display = ['pk','valor_inicial','valor_final','abierta','ruta',]

admin.site.register(Caja, CajaAdmin)
# Register your models here.

from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.views import View
from django.shortcuts import render, redirect, HttpResponseRedirect
from django.views.generic import ListView
from django.db.models import F, Sum
from caja.models import Caja
from rutas.models import Ruta
from movimientos.models import Movimiento
from clientes.models import Cliente, ClienteRuta
from ventas.models import Venta, VentaNoCobrado
from pagos.models import Pago
from pagos.forms import PagoForm
from django.contrib import messages

class CajaResumen(LoginRequiredMixin, ListView):
	model = Caja
	template_name = "bt_caja/caja_resumen.html"

	def get_queryset(self, **kwargs):
		queryset = super().get_queryset()
		return queryset.filter(pk=self.kwargs['caja']).first()

	def get_context_data(self, **kwargs):
		from movimientos.forms import MovimientoCrearForm
		context = super().get_context_data()
		context['form'] = MovimientoCrearForm()
		context['form2'] = PagoForm()
		context['ruta'] = self.get_queryset().ruta
		context['caja_pk'] = self.kwargs['caja']
		context['queryset'] = Caja.objects.filter(ruta=self.get_queryset().ruta).order_by("-pk")[:10]
		return context

class CajaPeriodoView(LoginRequiredMixin, View):
	def get(self, request, *args, **kwargs):
		from movimientos.forms import MovimientoCrearForm
		context = {}
		context['form'] = MovimientoCrearForm()
		start = request.GET.get("start")
		end = request.GET.get("end")
		cajas = Caja.objects.filter(ruta=self.kwargs['ruta'], created__date__range=(start, end))
		if cajas:
			clientes = Cliente.objects.filter(created__date__range=(start, end), ruta=self.kwargs['ruta'])
			context['ruta'] = self.kwargs['ruta']
			context['clientes'] = clientes
			context['cajas'] = cajas
			context['self'] = Ruta.objects.get(pk=context['ruta'])
			ventas_activas = Venta.objects.filter(no_cobrado=True, cliente_ruta__ruta=context['ruta'])
			data = []
			cont_ventas = 0
			cont_pagos = 0
			recaudo_pretendido = 0
			ruta_saldo = 0
			ventas_nuevas = 0
			gasto = 0
			retiro = 0
			ingreso = 0
			for data in cajas:
				cont_ventas += data.ventas_total()
				cont_pagos += data.pagos()
				ventas_nuevas += data.venta_set.all().count()
				gasto += data.gasto()
				retiro += data.retiro()
				ingreso += data.ingreso()

			for data in ventas_activas:
				recaudo_pretendido += data.cuota()
				ruta_saldo = data.saldo()

			caja_inicial = cajas.order_by("pk").first()
			caja_final = cajas.order_by("-pk").first()
			context['caja_inicial'] = caja_inicial.valor_inicial
			context['caja_final'] = caja_final.valor_final
			context['ventas'] = cont_ventas
			context['recaudo'] = cont_pagos
			context['gasto'] = gasto
			context['retiro'] = retiro
			context['ingreso'] = ingreso
			context['debido_cobrar'] = recaudo_pretendido
			context['ruta_saldo_inversion'] = ruta_saldo
			context['ventas_nuevas'] = ventas_nuevas
			context['start'] = start
			context['end'] = end
			return render(request, "bt_caja/caja_resumen_periodo.html", context)
		else:
			ruta = Ruta.objects.get(pk=self.kwargs['ruta'])
			messages.info(request, 'No existe ninguna caja en el rango de fecha montado')
			return redirect('caja:resumen_caja', ruta.ultima_caja().pk)
# Create your views here.

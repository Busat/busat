from django.db import models
from rutas.models import Ruta
from django.urls import reverse
from django_extensions.db.models import TimeStampedModel
from django.db.models import Sum, F
from django.utils import timezone

#Modelo Caja
class Caja(TimeStampedModel):
	valor_inicial = models.FloatField(default=0)
	valor_final = models.FloatField(default=0)
	abierta = models.BooleanField(default=True)
	ruta = models.ForeignKey(Ruta, on_delete =models.CASCADE)

	def caja_actual(self):
		data = Caja.objects.filter(ruta=self.ruta.pk).order_by("-pk").first()
		return data

	def ventas_total(self):
		ventas_total = self.venta_set.all().aggregate(Sum('valor'))
		if ventas_total['valor__sum'] != None:
			return ventas_total["valor__sum"]
		else:
			return 0


	def __str__(self):
		if self.abierta:
			return "Caja: %d >> Apertura: %s == Estado: Abierta"%(self.pk, self.created.date())
		else:
			return "Caja: %d >> Apertura: %s == Estado: Cerrada"%(self.pk, self.created.date())

	def movimientos_data(self):
		return self.movimientos().order_by("tipo_movimiento__descripcion")

	def nosincronizados(self):
		ventas_nosincronizadas = self.ventanocobrado_set.all().first()
		if ventas_nosincronizadas:
			return ventas_nosincronizadas.venta.all()
		else:
			return None

	def ventas_ruta(self):
		from ventas.models import Venta
		aux = 0
		ventas = Venta.objects.filter(no_cobrado=True, cliente_ruta__ruta=self.ruta).exclude(caja=self.pk)
		for data in ventas:
			aux+=data.cuota()
		return aux

	def ruta_saldo_inversion(self):
		from ventas.models import Venta
		aux = 0
		ventas = Venta.objects.filter(no_cobrado=True, cliente_ruta__ruta=self.ruta)
		for data in ventas:
			aux+=data.saldo()
		return aux

	def pagos(self):
		res = self.pago_set.all().aggregate(Sum('valor'))
		if res['valor__sum'] != None:
			return res["valor__sum"]
		else:
			return 0

	def si_pago(self):
		pagos = self.pago_set.filter(tipo_pago__descripcion__contains="DINERO").order_by("-valor")
		return pagos

	def no_pagos(self):
		pagos = self.pago_set.all().exclude(tipo_pago__descripcion__contains="DINERO")
		return pagos

	def movimientos(self):
		return self.movimiento_set.all()

	def gasto(self):
		gasto = self.movimientos()
		gasto = gasto.filter(tipo_movimiento__descripcion__contains ="GASTO").aggregate(Sum('valor'))
		if gasto['valor__sum'] != None:
			return gasto["valor__sum"]
		else:
			return 0

	def retiro(self):
		retiro = self.movimientos()
		retiro = retiro.filter(tipo_movimiento__descripcion__contains ="RETIRO").aggregate(Sum('valor'))
		if retiro['valor__sum'] != None:
			return retiro["valor__sum"]
		else:
			return 0

	def ingreso(self):
		ingreso = self.movimientos()
		ingreso = ingreso.filter(tipo_movimiento__descripcion__contains ="INGRESO").aggregate(Sum('valor'))
		if ingreso['valor__sum'] != None:
			return ingreso["valor__sum"]
		else:
			return 0

	def clientes(self):
		now = timezone.now()
		clientes = self.ruta.clienteruta_set.all()
		return clientes

	def clientes_inicio(self):
		now = timezone.now()
		clientes = self.ruta.cliente_set.all().filter(created__date__lt = now.date()).count()
		return clientes

	def clientes_nuevos(self):
		now = timezone.now()
		clientes = self.ruta.cliente_set.all().filter(created__date = now.date()).count()
		return clientes

	def total_clientes(self):
		data = self.clientes_nuevos()+self.clientes_inicio()
		return data

	def caja_final(self):
		suma = (self.pagos()+self.ingreso())-(self.ventas_total()+self.gasto()+self.retiro())
		return suma

	def total_final(self):
		return self.caja_final()+self.valor_inicial

	def save(self, *args, **kwargs):
		self.valor_final = self.total_final()
		super().save(*args, **kwargs)  # Call the "real" save() method.
# Create your models here.

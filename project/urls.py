"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from usuarios.views import Login, logout, PolitivasPrivacidad

from rest_framework.documentation import include_docs_urls



urlpatterns = [
    path('admin/', admin.site.urls),
    path('usuarios/', include('usuarios.urls')),
    path('',Login.as_view(), name="login" ),
    path('logout/', logout, name="logout"),
    path('rutas/', include('rutas.urls')),
    path('clientes/',include('clientes.urls')),
    path('ventas/',include('ventas.urls')),
    path('pagos/',include('pagos.urls')),
    path('caja/', include('caja.urls')),
    path('movimientos/', include('movimientos.urls')),
    path('retiros/', include('retiros.urls')),
    path('politicas_de_privacidad/',PolitivasPrivacidad.as_view() ,name="politivas_de_privacidad"),

    #URLS API
    path('docs/', include_docs_urls(title='Django API')),
    path('api/', include('api.urls')),


]

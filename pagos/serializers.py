from rest_framework.serializers import ModelSerializer, FloatField

from pagos.models import Motivo, TipoPago, Pago

class MotivoSerializer(ModelSerializer):
    class Meta:
        model = Motivo
        fields = '__all__'

class TipoPagoSerializer(ModelSerializer):
    class Meta:
        model = TipoPago
        fields = '__all__'

class PagoSerializer(ModelSerializer):
    class Meta:
        model = Pago
        fields = '__all__'

class PagoInformacionSerializer(ModelSerializer):
    saldo_pago = FloatField(source = 'saldo')
    class Meta:
        model = Pago
        fields = '__all__'

from django.urls import path
from pagos.views import ListarPago, CrearPago, EditarPago, PagoEliminar
from pagos.ajax import eliminar_list_pagos

app_name = "pagos"

urlpatterns = [
	path("listar/<int:id_venta>/", ListarPago.as_view(), name = "listar_pagos"),
	path("gestionar/", CrearPago.as_view(), name = "gestionar_pago" ),
	path("gestionar/<int:caja>", CrearPago.as_view(), name = "gestionar_pago_caja" ),
	path('editar/<int:pk>/', EditarPago.as_view(), name = "editar_pago"),
	path('editar/<int:pk>/<int:caja>', EditarPago.as_view(), name = "editar_pago_caja"),
	path('eliminar/<int:pk>/', PagoEliminar.as_view(), name = 'eliminar_pago'),
	path('eliminar/<int:pk>/<int:caja>', PagoEliminar.as_view(), name = 'eliminar_pago_caja'),
	path('pagos_list/eliminar/', eliminar_list_pagos)

]
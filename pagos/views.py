from django.shortcuts import render, redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from pagos.models import Pago
from ventas.models import Venta
from pagos.forms import PagoForm, PagoEditaForm

from django.urls import reverse_lazy
from django.views.generic.edit import FormView, UpdateView, CreateView, DeleteView
from django.views.generic import ListView

class CrearPago(LoginRequiredMixin, CreateView):
	model = Pago
	template_name = "bt_pagos/editar_pago.html"
	form_class = PagoForm

	def get_success_url(self):
		if 'caja' in self.kwargs:
			return reverse_lazy("caja:resumen_caja", kwargs={'caja':self.kwargs['caja']})
		else:
			return self.object.url_absoluta()

class EditarPago(LoginRequiredMixin, UpdateView):
	model = Pago
	template_name = "bt_pagos/editar_pago.html"
	form_class = PagoEditaForm

	def get_success_url(self):
		if 'caja' in self.kwargs:
			return reverse_lazy("caja:resumen_caja", kwargs={'caja':self.kwargs['caja']})
		else:
			return self.object.url_absoluta()

	def get_context_data(self, **kwargs):
		context = super().get_context_data()
		if 'caja' in self.kwargs:
			context['caja'] = self.kwargs['caja']
		return context

class ListarPago(LoginRequiredMixin, ListView):
	model = Pago
	template_name = "bt_pagos/listar_pagos.html"

	def get_queryset(self, **kwargs):
		queryset = super().get_queryset()
		return queryset.filter(venta=self.kwargs['id_venta'])

	def get_context_data(self, **kwargs):
		context = super().get_context_data()
		return context

class PagoEliminar(LoginRequiredMixin, DeleteView):
	model = Pago

	def get(self, reques, *args, **kwargs):
		objeto = Pago.objects.get(pk=self.kwargs['pk'])
		venta = objeto.venta.pk
		objeto.delete()
		if 'caja' in self.kwargs:
			return redirect("caja:resumen_caja", caja=self.kwargs['caja'])
		else:
			return redirect("pagos:listar_pagos", id_venta=venta)
# Create your views here.

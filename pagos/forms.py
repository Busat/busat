from django import forms
from pagos.models import Pago

class PagoForm(forms.ModelForm):
	class Meta:
		model = Pago
		fields = ['valor','tipo_pago','motivo', 'venta', 'caja']
		labels = {
			'valor':'Cantidad a pagar',
			'tipo_pago':'Tipo de pago',
			'motivo':'Motivo de no pago',
			'venta':'Venta',
			'caja':'Caja',
		}
		widgets = {
			'valor':forms.TextInput(attrs = {'class':'form-control', 'type':'number'}),
			'tipo_pago':forms.Select(attrs = {'class':'form-control motivo',}),
			'motivo':forms.Select(attrs = {'class':'form-control' }),
			'venta':forms.TextInput(attrs = {'class':'form-control', 'type':'hidden'}),
			'caja':forms.TextInput(attrs = {'class':'form-control', 'type':'hidden', 'id':'cajita'}),
		}

class PagoEditaForm(forms.ModelForm):
	class Meta:
		model = Pago
		fields = ['valor']
		labels = {
			'valor':'Cantidad a pagar',
		}
		widgets = {
			'valor':forms.TextInput(attrs = {'class':'form-control', 'type':'number'}),
		}
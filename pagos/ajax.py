from django.http import JsonResponse
from pagos.models import Pago
import json


def eliminar_list_pagos(request):
	action = request.GET.get("type")
	if action == "eliminar":
		li = request.GET.getlist("list_pagos[]")
		datos_list = json.loads(li[0])
		for x in li:
			pago = Pago.objects.get(pk=int(x))
			pago.delete()
	return JsonResponse({'res':True})
			
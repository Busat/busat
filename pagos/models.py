from django.db import models
from ventas.models import Venta
from caja.models import Caja
from django.urls import reverse
from django.db.models import Sum, Count
from django_extensions.db.models import TimeStampedModel

#Modelo Motivo del no pago
class Motivo(models.Model):
	descripcion = models.CharField(max_length=50)
	def __str__(self):
		return self.descripcion

#Modelo de Tipo Pago
class TipoPago(models.Model):
	descripcion = models.CharField(max_length=20)
	motivo = models.BooleanField(default=False)
	def __str__(self):
		return self.descripcion

#Modelo Pago
class Pago(TimeStampedModel):
	valor = models.FloatField(default=0)
	tipo_pago = models.ForeignKey(TipoPago, on_delete = models.CASCADE)
	motivo = models.ForeignKey(Motivo, on_delete = models.CASCADE, null=True, blank=True)
	venta = models.ForeignKey(Venta, on_delete = models.CASCADE)
	caja = models.ForeignKey(Caja, on_delete = models.CASCADE)

	def url_absoluta(self):
		return reverse("pagos:listar_pagos", kwargs={'id_venta':self.venta.pk})

	def saldo(self):
		data = Pago.objects.filter(venta=self.venta, pk__lte=self.pk).aggregate(Sum('valor'))
		if data['valor__sum']:
			return self.venta.valor_venta_interes()-data['valor__sum']
		else:
			return self.venta.valor_venta_interes()

	def delete(self, *args, **kwargs):
		no_sincro = self.caja.ventanocobrado_set.all().order_by("-pk").first()
		if no_sincro:
			no_sincro.venta.add(self.venta)
			suma_pagos = self.venta.suma_pagos().aggregate(Sum('valor'))
			if (self.venta.saldo()  + self.valor > 0):
				self.venta.no_cobrado = True
				self.venta.save()
		super().delete(*args, **kwargs)  # Call the "real" delete() method.

	def save(self, *args, **kwargs):
		ventas = self.caja.ventanocobrado_set.all().order_by("-pk").first()
		if ventas:
			ventas.venta.remove(self.venta)
			validardor = ventas.venta.all()
		pagos = Pago.objects.filter(caja=self.caja, venta=self.venta).count()
		if pagos == 0:
			super().save(*args, **kwargs)  # Call the "real" save() method.
		try:
			if (self.venta.saldo() <= 0):
				self.venta.no_cobrado = False
				self.venta.save()
		except TypeError:
			pass

	def duplicada(self):
		pagos = Pago.objects.filter(caja=self.caja, venta=self.venta).count()
		if pagos > 1 :
			return True
		else:
			return False

# Create your models here.

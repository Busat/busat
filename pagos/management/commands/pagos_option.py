from django.core.management.base import BaseCommand, CommandError
from pagos.models import TipoPago, Motivo

class Command(BaseCommand):
	def handle(self, *args, **options):
		if Motivo.objects.all().count() == 0:
			tupla = ('CLAVO', 'CLIENTE NO ENCONTRADO', 'CLIENTE SIN DINERO', 'INCAPAZ DE COBRAR', 'LOCAL CERRADO')
			for x in tupla:
				m = Motivo(descripcion = x)
				m.save()
		if TipoPago.objects.all().count() == 0:
			tupla = ('DINERO', 'SIN PAGO')
			for x in tupla:
				pag = TipoPago(descripcion = x)
				objeto = pag.save()
				if x == "SIN PAGO":
					for data in Motivo.objects.all():
						pag = TipoPago(descripcion = x, motivo=True)

from django.contrib import admin
from pagos.models import Pago, TipoPago, Motivo

#Lista todos los pagos en el admin de django
class PagoAdmin(admin.ModelAdmin):
	list_display = ['pk', 'valor','tipo_pago','motivo','venta','caja',]

#Lista todos los tipos de pago en el admin de django
class TipoPagoAdmin(admin.ModelAdmin):
	list_display = ['pk', 'descripcion', 'motivo', ]

#Lista todos los motivos de no pago en el admin de django
class MotivoAdmin(admin.ModelAdmin):
	list_display = ['pk', 'descripcion', ]

admin.site.register(Pago, PagoAdmin)
admin.site.register(TipoPago, TipoPagoAdmin)
admin.site.register(Motivo, MotivoAdmin)
# Register your models here.

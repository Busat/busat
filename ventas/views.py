from django.shortcuts import render, redirect

from django.urls import reverse_lazy, reverse
from django.views.generic.edit import FormView, UpdateView, CreateView, DeleteView
from django.views.generic import ListView
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from caja.models import Caja
from ventas.models import Venta, VentaNoCobrado
from clientes.models import ClienteRuta
from ventas.forms import VentaForm, VentaActualizarForm
from pagos.forms import PagoForm
from django.views import View

class VentaLisar(LoginRequiredMixin, ListView):
	model = Venta
	template_name="bt_ventas/listar_ventas.html"

	def get_queryset(self, **kwargs):
		queryset = super().get_queryset()
		if'cliente' in self.kwargs:
			return queryset.filter(cliente_ruta=self.kwargs['cliente'])
		else:
			return queryset.filter(cliente_ruta__ruta=self.kwargs['idruta'])

	def get_context_data(self, **kwargs):
		context = super().get_context_data()
		context['form'] = PagoForm()
		return context

class VentasActivas(LoginRequiredMixin, View):
	def get(self, request, *args, **kwargs):
		context = {}
		context['object_list'] = Venta.objects.filter(cliente_ruta__ruta=self.kwargs['idruta'], no_cobrado=True)
		context['form'] = PagoForm()
		return render(request, "bt_ventas/listar_ventas_activas.html", context)

class VentaCrear(LoginRequiredMixin, FormView):
	form_class = VentaForm
	cliente = None
	template_name = "bt_clientes/listar_clientes.html"

	def get_success_url(self):
		return redirect("ventas:listar_ventas", cliente=self.cliente)

	def form_valid(self, form):
		cuotas = form.cleaned_data.get('cuotas')	
		interes = form.cleaned_data.get('interes')
		valor = form.cleaned_data.get('valor')
		cliente_ruta = form.cleaned_data.get('id_cliente_ruta')
		cliente_ruta = ClienteRuta.objects.get(pk=int(cliente_ruta))
		self.cliente=cliente_ruta.pk
		if not cliente_ruta.ruta.ultima_caja():
			messages.success(self.request, 'No hay una Caja ACTIVA para esta ruta, por favor active la ruta para crearle una caja!')
			return self.get_success_url()
		caja = Caja.objects.filter(ruta=cliente_ruta.ruta.pk).order_by("-pk").first()
		venta = Venta(cuotas=cuotas, interes=interes, valor=valor, cliente_ruta=cliente_ruta, caja=caja)
		venta.save()
		return self.get_success_url()

class VentaActualizar(LoginRequiredMixin, UpdateView):
	model = Venta
	form_class = VentaActualizarForm
	template_name = "bt_ventas/editar_venta.html"
	success_url = reverse_lazy("rutas:listar_rutas")

	def get_success_url(self):
		if 'caja' in self.kwargs:
			return reverse_lazy("caja:resumen_caja", kwargs={'caja':self.kwargs['caja']})
		venta = Venta.objects.get(pk=self.kwargs['pk'])
		data = venta.cliente_ruta.pk
		return reverse_lazy("ventas:listar_ventas", kwargs={'cliente':data})

	def get_context_data(self, **kwargs):
		context = super().get_context_data()
		if 'caja' in self.kwargs:
			context['caja'] = self.kwargs['caja']
		return context

class VentaEliminar(LoginRequiredMixin, DeleteView):
	model = Venta

	def get(self, reques, *args, **kwargs):
		objeto = Venta.objects.get(pk=self.kwargs['pk'])
		cliente = objeto.cliente_ruta.cliente.pk
		objeto.delete()
		if 'caja' in self.kwargs:
			return redirect("caja:resumen_caja", caja=self.kwargs['caja'])
		else:
			return redirect("ventas:listar_ventas", cliente=cliente)
# Create your views here.

from django.contrib import admin
from ventas.models import Venta, VentaNoCobrado

#Lista todas las ventas en el admin de django
class VentaAdmin(admin.ModelAdmin):
	list_display = ['pk','cuotas','interes','valor','no_cobrado','saldo','cliente_ruta','caja',]

class VentaNocobradoAdmin(admin.ModelAdmin):
	list_display = ['pk','caja']
	filter_horizontal = ('venta',)

admin.site.register(Venta, VentaAdmin)
admin.site.register(VentaNoCobrado, VentaNocobradoAdmin)

# Register your models here.

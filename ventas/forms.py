from django import forms
from ventas.models import Venta
from clientes.models import ClienteRuta

class VentaForm(forms.Form):
	cuotas = forms.CharField(label='A cuantos dias?', max_length=100, widget=forms.TextInput(attrs={'class':'form-control','type':'number'}))
	interes = forms.CharField(label='Porcentaje %', max_length=100, widget=forms.TextInput(attrs={'class':'form-control','type':'number'}))
	valor = forms.CharField(label='Cantidad a prestar', max_length=100, widget=forms.TextInput(attrs={'class':'form-control','type':'number'}))
	id_cliente_ruta = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','type':'hidden', 'value':''}))
	


class VentaActualizarForm(forms.ModelForm):
	class Meta:
		model = Venta
		fields = [ 'cuotas','interes','valor']
		exclude = ['cliente_ruta', 'caja']
		labels = {
			'cuotas':'A cuantos dias?',
			'interes':'Porcentaje %',
			'valor':'Cantidad a prestar',
		}
		widgets ={
			'cuotas': forms.TextInput(attrs={'class':'form-control', 'type':'number'}),
			'interes': forms.TextInput(attrs={'class':'form-control', 'type':'number'}),
			'valor': forms.TextInput(attrs={'class':'form-control', 'type':'number'}),
		}
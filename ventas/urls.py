from django.urls import path
from ventas.views import VentaCrear, VentaLisar, VentaActualizar, VentaEliminar, VentasActivas

app_name="ventas"

urlpatterns = [
	path('listar/<int:cliente>/',VentaLisar.as_view(), name ="listar_ventas" ),
	path('crear/', VentaCrear.as_view(), name="crear_venta"),
	path('editar/<int:pk>/', VentaActualizar.as_view(), name="actualizar_venta"),
	path('editar/<int:pk>/<int:caja>', VentaActualizar.as_view(), name="actualizar_venta_caja"),
	path('eliminar/<int:pk>/', VentaEliminar.as_view(), name="eliminar_venta"),
	path('eliminar/<int:pk>/<int:caja>', VentaEliminar.as_view(), name="eliminar_venta_caja"),
	path('listar/ruta/<int:idruta>', VentaLisar.as_view(), name ="listar_ventas_ruta"),
	path('listar/activas/<int:idruta>', VentasActivas.as_view(), name = 'listar_ventas_activas'),
]
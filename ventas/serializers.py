from rest_framework.serializers import ModelSerializer, FloatField, IntegerField, BooleanField

from ventas.models import Venta, VentaNoCobrado

from clientes.serializers import ClienteRutaSerializer

class VentaSerializer(ModelSerializer):
    saldo_actual = FloatField(source = 'saldo')
    cuota_actual = IntegerField(source = 'dias')
    valor_total = FloatField(source = 'valor_venta_interes')
    venta_cobrada = BooleanField(source = 'pago_actual_venta')

    class Meta:
        model = Venta
        fields='__all__'


class CrearVentaSerializer(ModelSerializer):

    class Meta:
        model = Venta
        fields='__all__'

class VentaNoCobradoSerializer(ModelSerializer):
    class Meta:
        model = VentaNoCobrado
        fields='__all__'

from django.db import models
from clientes.models import ClienteRuta
from caja.models import Caja
from django.db.models import Sum, Count
from django_extensions.db.models import TimeStampedModel
#Modelo Venta
class Venta(TimeStampedModel):
	cuotas = models.IntegerField(default = 10)
	interes = models.IntegerField(default = 10)
	valor = models.FloatField(default=0)
	no_cobrado = models.BooleanField(default=True)
	cliente_ruta = models.ForeignKey(ClienteRuta, on_delete = models.CASCADE)
	caja = models.ForeignKey(Caja, on_delete=models.CASCADE)

	def suma_pagos(self):
		suma_pagos = self.pago_set.all()
		return suma_pagos

	def valor_venta_interes(self):
		porcentaje = self.interes/100
		interes = porcentaje*self.valor
		saldo = self.valor+interes
		return saldo

	def gestionar_pago(self):
		data = 0
		caja = self.cliente_ruta.ruta.ultima_caja()
		if caja:
			data = caja.pago_set.all().filter(venta=self.pk).count()
		return data

	def saldo(self):
		suma_pagos=self.suma_pagos().aggregate(Sum('valor'))
		if suma_pagos['valor__sum']:
			return self.valor_venta_interes()-suma_pagos['valor__sum']
		else:
			return self.valor_venta_interes()

	def dias(self):
		dias = self.suma_pagos().count()
		return dias

	def cuota(self):
		res = self.valor_venta_interes()/self.cuotas
		return res

	def categoria_venta(self):
		pagos= self.pago_set.all().filter(tipo_pago__descripcion__contains="SIN PAGO").count()
		if self.cuotas>=12:		
			if pagos>=5 and pagos<=9:
				return "#C6BA21"
			elif pagos>=10:
				return "#DA0D0D"
			return "#06C106"
		else:
			if pagos>=3 and pagos<=6:
				return "#C6BA21"
			elif pagos>=7:
				return "#DA0D0D"
			return "#06C106"

	def pagos(self):
		pagos = self.pago_set.all().aggregate(Sum('valor'))['valor__sum']
		if pagos:
			return pagos
		else:
			return 0

	def ultimo_pago(self):
		pago = self.pago_set.filter(tipo_pago__descripcion__contains="DINERO").order_by("-pk").first()
		return pago

	def pago_actual_venta(self):
		pago = self.pago_set.filter(caja=self.cliente_ruta.ruta.ultima_caja().pk)
		if pago:
			return True
		return False

	def __str__(self):
		return "%s"%self.pk
#Modelo de Venta No Cobrada (No sincronizados)
class VentaNoCobrado(models.Model):
	venta = models.ManyToManyField(Venta, blank=True)
	caja = models.ForeignKey(Caja, on_delete=models.CASCADE)
# Create your models here.

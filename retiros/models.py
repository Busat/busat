from django.db import models
from usuarios.models import User
from rutas.models import Ruta
from django_extensions.db.models import TimeStampedModel

#Modelo Transacciones (Retiros)
class Transaccion(TimeStampedModel):
	rutas = models.ForeignKey(Ruta, on_delete=models.CASCADE)
	valor = models.FloatField(default=0)
	cuenta_bancaria = models.CharField(max_length = 25)
	usuario = models.ForeignKey(User, on_delete=models.CASCADE)

#Modelo Inversion
class Inversion(TimeStampedModel):
	ruta_destino = models.ForeignKey(Ruta, on_delete=models.CASCADE, related_name='ruta_destino')
	valor = models.FloatField(default=0)
	ruta_origen = models.ForeignKey(Ruta, on_delete=models.CASCADE, related_name = 'ruta_origen', blank=True, null=True)
	origen = models.TextField(max_length = 30, blank=True, null=True)
	usuario = models.ForeignKey(User, on_delete=models.CASCADE)


# Create your models here.


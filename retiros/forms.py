from django import forms
from retiros.models import Transaccion, Inversion
from rutas.models import Ruta

class TransaccionForm(forms.ModelForm):
	class Meta:
		model = Transaccion
		fields = '__all__'
		widgets = {
			'rutas': forms.Select(attrs={'class':'form-control'}),
			'valor': forms.TextInput(attrs={'class':'form-control', 'type':'number'}),
			'cuenta_bancaria': forms.TextInput(attrs={'class':'form-control', }),
			'usuario': forms.Select(attrs={'class':'form-control'}),
		}
	def __init__(self, *args, **kwargs):
		super(TransaccionForm, self).__init__(*args, **kwargs)
		if kwargs['initial']:
			self.fields['rutas'].queryset = Ruta.objects.filter(imei__comunidad=(kwargs['initial']['user'].pk))

class TransaccionEditar(forms.ModelForm):
	class Meta:
		model = Transaccion
		fields = ['rutas', 'valor', 'cuenta_bancaria']
		labels = {
			'rutas': 'A que ruta se le hara el retiro?',
			'valor': 'Valor del retiro',
			'cuenta_bancaria': 'Que cuenta o lugar se hara el deposito',
		}
		widgets = {
			'rutas': forms.Select(attrs={'class':'form-control'}),
			'valor': forms.TextInput(attrs={'class':'form-control', 'type':'number'}),
			'cuenta_bancaria': forms.TextInput(attrs={'class':'form-control', }),
		}
	def __init__(self, *args, **kwargs):
		super(TransaccionEditar, self).__init__(*args, **kwargs)
		if kwargs['initial']:
			self.fields['rutas'].queryset = Ruta.objects.filter(imei__comunidad=(kwargs['initial']['user'].pk))


class InversionForm(forms.ModelForm):
	class Meta:
		model = Inversion
		fields = '__all__'
		widgets = {
			'ruta_destino': forms.Select(attrs={'class':'form-control'}),
			'valor': forms.TextInput(attrs={'class':'form-control', 'type':'number'}),
			'ruta_origen': forms.Select(attrs={'class':'form-control'}),
			'origen': forms.TextInput(attrs={'class':'form-control'}),
			'usuario': forms.Select(attrs={'class':'form-control'}),
		}

	def __init__(self, *args, **kwargs):
		super(InversionForm, self).__init__(*args, **kwargs)
		if kwargs['initial']:
			self.fields['ruta_destino'].queryset = Ruta.objects.filter(imei__comunidad=(kwargs['initial']['user'].pk))
			self.fields['ruta_origen'].queryset = Ruta.objects.filter(imei__comunidad=(kwargs['initial']['user'].pk))


class InversionEditar(forms.ModelForm):
	class Meta:
		model = Inversion
		fields = ['ruta_destino', 'valor', 'ruta_origen', 'origen']
		labels = {
			'ruta_destino':'Ruta de Destino',
			'valor':'Valor a Depositar',
			'ruta_origen':'Ruta de origen',
			'origen':'O origen de la inversion',
		}
		widgets = {
			'ruta_destino': forms.Select(attrs={'class':'form-control'}),
			'valor': forms.TextInput(attrs={'class':'form-control', 'type':'number'}),
			'ruta_origen': forms.Select(attrs={'class':'form-control'}),
			'origen': forms.TextInput(attrs={'class':'form-control'}),
		}
	def __init__(self, *args, **kwargs):
		super(InversionEditar, self).__init__(*args, **kwargs)
		if kwargs['initial']:
			self.fields['ruta_destino'].queryset = Ruta.objects.filter(imei__comunidad=(kwargs['initial']['user'].pk))
			self.fields['ruta_origen'].queryset = Ruta.objects.filter(imei__comunidad=(kwargs['initial']['user'].pk))
from django.contrib import admin
from retiros.models import Transaccion, Inversion

#Lista todas las transacciones en el admin de django
class TransaccionAdmin(admin.ModelAdmin):
	list_display = ['pk','rutas','valor','cuenta_bancaria','usuario',]

#Lista todas las inversiones en el admin de django
class InvercionAdmin(admin.ModelAdmin):
	list_display = ['pk','ruta_destino','valor','ruta_origen','origen','usuario',]

admin.site.register(Transaccion, TransaccionAdmin)
admin.site.register(Inversion, InvercionAdmin)
# Register your models here.

from django.urls import path
from retiros.views import TransaccionCrear, TransaccionListar, TransaccionEditar, TransaccionEliminar, \
							InversionListar, InversionCrear, InversionEditar, Buscar

app_name = "retiros"

urlpatterns = [
	path('listar/', TransaccionListar.as_view(), name = "listar_retiros"),
	path('crear/', TransaccionCrear.as_view(), name = "crear_retiro"),
	path('editar/<int:pk>/', TransaccionEditar.as_view(), name = "editar_retiro"),
	path('eliminar/<int:pk>', TransaccionEliminar.as_view(), name = "eliminar_retiro"),

	path('inversiones/listar', InversionListar.as_view(), name = "listar_inversiones"),
	path('inversiones/crear/', InversionCrear.as_view(), name = "crear_inversion"),
	path('inversiones/editar/<int:pk>/', InversionEditar.as_view(), name = "editar_inversion"),

	path('busqueda/<slug:busqueda>/', Buscar.as_view(), name="busqueda"),
]
from rest_framework.serializers import ModelSerializer, DateTimeField

from retiros.models import Transaccion, Inversion

from rutas.serializers import RutaSerializer
from usuarios.serializers import UsuarioSerializer

class TransaccionSerializer(ModelSerializer):
    created = DateTimeField(format = '%d/%m/%Y')
    rutas = RutaSerializer()
    usuario = UsuarioSerializer()
    class Meta:
        model=Transaccion
        fields='__all__'

class CrearTransaccionSerializer(ModelSerializer):
    class Meta:
        model=Transaccion
        fields='__all__'

class InversionSerializer(ModelSerializer):
    created = DateTimeField(format = '%d/%m/%Y')
    ruta_destino = RutaSerializer()
    ruta_origen = RutaSerializer()
    usuario = UsuarioSerializer()
    class Meta:
        model=Inversion
        fields='__all__'

class CrearInversionSerializer(ModelSerializer):
    class Meta:
        model=Inversion
        fields='__all__'

from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from retiros.models import Transaccion, Inversion
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import ListView
from retiros.forms import TransaccionForm, TransaccionEditar, InversionForm, InversionEditar
from django.urls import reverse_lazy, reverse
from django.shortcuts import render, redirect
from django.views import View

class TransaccionCrear(LoginRequiredMixin, CreateView):
	model = Transaccion
	form_class = TransaccionForm
	template_name = "bt_retiros/listar_retiros.html"
	success_url = reverse_lazy('retiros:listar_retiros')

class TransaccionListar(LoginRequiredMixin, ListView):
	model = Transaccion
	template_name = "bt_retiros/listar_retiros.html"

	def get_queryset(self):
		queryset = super().get_queryset()
		if self.request.user.has_perm("usuarios.is_secretarias"):
			queryset.filter(usuario=self.request.user.pk)
		return queryset.filter(usuario__comunidad=self.request.user.comunidad.pk)

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['form'] = TransaccionForm(initial={'user':self.request.user.comunidad})
		return context

class TransaccionEditar(LoginRequiredMixin, UpdateView):
	model = Transaccion
	form_class = TransaccionEditar
	template_name = "bt_retiros/editar_retiros.html"
	success_url = reverse_lazy('retiros:listar_retiros')

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		tra = Transaccion.objects.get(pk=self.kwargs['pk'])
		context['form'] = self.form_class(instance=tra, initial={'user':self.request.user.comunidad})
		return context

class TransaccionEliminar(LoginRequiredMixin, DeleteView):
	model = Transaccion
	success_url = reverse_lazy("retiros:listar_retiros")

	def get(self, request, *args, **kargs):
		objeto = Transaccion.objects.get(pk=self.kwargs['pk'])
		objeto.delete()
		return redirect("retiros:listar_retiros")

class InversionListar(LoginRequiredMixin, ListView):
	model = Inversion
	template_name = "bt_retiros/listar_inversiones.html"

	def get_queryset(self):
		queryset = super().get_queryset()
		if self.request.user.has_perm("usuarios.is_secretarias"):
			queryset.filter(usuario=self.request.user.pk)
		return queryset.filter(usuario__comunidad=self.request.user.comunidad.pk)

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['form'] = InversionForm(initial={'user':self.request.user.comunidad})
		return context

class InversionCrear(LoginRequiredMixin, CreateView):
	model = Inversion
	form_class = InversionForm
	template_name = "bt_retiros/listar_inversiones.html"
	success_url = reverse_lazy('retiros:listar_inversiones')

class InversionEditar(LoginRequiredMixin, UpdateView):
	model = Inversion
	form_class = InversionEditar
	template_name = "bt_retiros/editar_inversiones.html"
	success_url = reverse_lazy('retiros:listar_inversiones')

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		inv = Inversion.objects.get(pk=self.kwargs['pk'])
		context['form'] = self.form_class(instance=inv, initial={'user':self.request.user.comunidad})
		return context

class TransaccionEliminar(LoginRequiredMixin, DeleteView):
	model = Inversion
	success_url = reverse_lazy("retiros:listar_inversiones")

	def get(self, request, *args, **kargs):
		objeto = Inversion.objects.get(pk=self.kwargs['pk'])
		objeto.delete()
		return redirect("retiros:listar_inversiones")

class Buscar(LoginRequiredMixin, View):
	def get(self, request, *args, **kwargs):
		context = {}
		inicio = self.request.GET.get('inicio')
		fin = self.request.GET.get("fin")
		busqueda = self.kwargs['busqueda']
		if busqueda == 'transaccion':
			if self.request.user.has_perm("usuarios.is_secretarias"):
				context['object_list'] = Transaccion.objects.filter(created__date__range=(inicio, fin), usuario__comunidad=self.request.user.comunidad.pk)
				return render(request, "bt_retiros/listar_retiros.html", context)
			context['object_list'] = Transaccion.objects.filter(created__date__range=(inicio, fin))
			return render(request, "bt_retiros/listar_retiros.html", context)
		elif busqueda == 'inversion':
			if self.request.user.has_perm("usuarios.is_secretarias"):
				context['object_list'] = Inversion.objects.filter(created__date__range=(inicio, fin), usuario__comunidad=self.request.user.comunidad.pk)
				return render(request, "bt_retiros/listar_inversiones.html", context)
			context['object_list'] = Inversion.objects.filter(created__date__range=(inicio, fin))
			return render(request, "bt_retiros/listar_inversiones.html", context)
# Create your views here.
